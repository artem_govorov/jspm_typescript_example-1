module Concerns

  module HasMutableAttributes

    extend ActiveSupport::Concern


    class AttributeList
      def initialize(attributes)
        @attributes = attributes;
      end

      def names
        results = []
        @attributes.each do |entry|
          case entry
            when Hash
              results += entry.keys
            when Symbol
              results << entry
            else
              raise "Expected a Symbol or Hash, got #{entry.inspect}"
          end
        end
        results
      end

      def as_permit_params
        @attributes
      end

    end


    included do
      class_attribute :_mutable_attribute_list
      class_attribute :_create_attribute_list
      self._mutable_attribute_list = []
      self._create_attribute_list = []
    end

    def mutable_attributes
      self.class.mutable_attributes
    end

    def create_attributes
      self.class.create_attributes
    end

    module ClassMethods
      def has_mutable_attributes(*attrs)
        self._mutable_attribute_list += _verify_attrs(attrs)
        nil
      end

      def has_create_attributes(*attrs)
        self._create_attribute_list += _verify_attrs(attrs)
        nil
      end

      def mutable_attributes
        AttributeList.new(self._mutable_attribute_list)
      end

      def create_attributes
        AttributeList.new(self._mutable_attribute_list + self._create_attribute_list)
      end

      def _verify_attrs(attrs)
        attrs.each do |entry|
          Assert.truthy!(entry.is_a?(Symbol) || entry.is_a?(Hash))
        end
        attrs
      end

    end

  end

end