module Api
  module V1

    ##
    # Controller syntax
    # Restrict Fields: ?fields=a,b,c
    # Sort: sort=name,-name
    # Include: embed=areas, devices
    class SessionsController <  ::Api::ResourceController

      resource class: Session
      belongs_to :concert

      index role: :any
      create role: :any
      update role: :any
      destroy role: :any

    end
  end

end