module Api
  module V1
    module Auth

      ##
      # Controller syntax
      # Restrict Fields: ?fields=a,b,c
      # Sort: sort=name,-name
      # Include: embed=areas, devices
      class UsersController < ::Api::ResourceController

        resource class: ::Auth::User

        index role: :any
        create role: :any
        update role: :any
        destroy Action::DestroyUserAction, role: :any

      end

    end

  end
end