module Api
  module V1
    module Auth


      # Temp until Knock lets me configure the user data directly.
      # This is a cut and paste of the standard knock controller.
      class AuthTokensController < ResourceController

        class LoginForm
          include Virtus.model
          include ActiveModel::Validations

          def self.i18n_scope
            :activerecord
          end

          attribute :username, String
          attribute :password, String

          validates_presence_of :username, :password
        end


        def self.schema_key
          'auth_tokens'
        end

        class CreateTokenAction < ApiActions::Action::CollectionAction

          http_method :post

          class IncorrectUsernameOrPassword < ApiActions::Errors::Base
            def initialize(payload={})
              super(422, payload)
            end
          end

          raises :validation_error, IncorrectUsernameOrPassword

          def on_execute
            form = LoginForm.new(login_params)

            unless form.valid?
              raise_validation_error!(form.errors)
            end

            user = ::Knock.current_user_from_handle.call login_params[::Knock.handle_attr]

            raise_incorrect_username_or_password! unless user && user.authenticate(login_params[:password])

            auth_token = CreateTokenAction.create_token_for(user)

            respond_with(auth_token)
          end

          def login_params
            params.require(:auth_token).permit(::Knock.handle_attr, :password)
          end

          def self.create_token_for(user)
            ::Auth::AuthToken.new(user)
          end

        end

        resource class: ::Auth::AuthToken, identified_by: false

        define_action :create, CreateTokenAction, role: :any

        def authorized_user_for!(role)
          #   do nothing as we're the login controller
        end


      end

    end
  end
end