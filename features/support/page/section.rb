module Page

  class Section < SitePrism::Section

    include ::Page::HasEnhancedSection
    include Capybara::RSpecMatchers

    def inner_html
      root_element['innnerHTML']
    end

    def find(*args)
      root_element.find(*args)
    end

  end
end