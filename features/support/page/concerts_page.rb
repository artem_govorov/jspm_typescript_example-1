module Page

  class ConcertsPage < ::Page::Base
    set_url '/#/concerts'


    def click_the_button
      self.find('button.md-button').click
    end
  end

end