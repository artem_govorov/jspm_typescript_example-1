require_relative 'has_widgets'
require_relative 'section'

module Page
  class Base < SitePrism::Page

    include ::Page::HasEnhancedSection

    def self.dialog(dialog_class_or_name, optional_dialog_class = nil)
      if dialog_class_or_name.is_a?(Symbol) || dialog_class_or_name.is_a?(String)
        dialog_name = dialog_class_or_name
        dialog_class = optional_dialog_class
      else
        dialog_name = dialog_class_or_name.name.demodulize.underscore
        dialog_class = dialog_class_or_name
      end
      selector = dialog_class.selector || ".#{dialog_class.name.demodulize.underscore.dasherize}"
      section(dialog_name, dialog_class, "#{selector}")
    end

    def self.alert_dialog
      dialog(::Page::Material::AlertDialog)
    end

    section :top_bar, 'main-nav' do
      section :user_profile, '#user-profile' do
        def open_menu
          find('.md-button.menu-toggle').click
        end
        element :logout_button, :xpath, "//*[contains(@command-id, 'logout-command')]"
      end

    end

    section :ribbon, 'md-toolbar.ribbon' do
      element :breadcrumbs, 'la-nav-breadcrumbs'
      # widget :tabs, Page::Components::Tabs, 'md-tabs'
    end

    def current_section
      @current_section || self
    end

    def enter_section(section)
      self.send("wait_until_#{section}_visible")
      @current_section = self.send(section)
      sleep 0.2 # give animations a chance to buttons don't get missed.
    end

    def clear_current_section
      @current_section = nil
    end

    def focus_on(section)
      self.current_section.send("wait_until_#{section}_visible")
      @focused_section = self.current_section.send(section)
    end

    def blur
      @focused_section = nil
    end

    def focused_section
      @focused_section || self.current_section
    end


    # NOTE: the below is all bogus. The approach now is:
    # 1. Each step that opens a dialog should call page.enter_section(<name>) with the name of the
    #    section that is now showing. i.e. 'When I open the blah dialog' should call enter_section
    #    with blah_dialog (where blah_dialog is the name of the section on the page object.)
    # 2. The generic dialog & form steps etc then call @page.current_section and perform all their
    #    actions on that.

    def enter_scope(scope)
      scopes << scope
    end

    def scopes
      @scopes ||= []
    end

    def within_scope(name, &block)
      scopes << name
      # we'll only create the scope when the test askes for it.  Otherwise we're resolving
      # too early (I was getting elements not found issues).
      if block.arity == 0
        block.call
      else
        # someone has asked for the scope object so we'll try and get it.
        page_element = self
        # navigate down our graph of sections and widgets
        scopes.each do |n|
          method = n.downcase.split.join('_').to_sym
          # page_element.send("wait_for_#{method}")
          page_element = page_element.send(method)
        end
        # if were a section or page return our root element, otherwise we're already a capybara element
        scope = page_element.try(:root_element) || page_element

        page.within(scope) do
          block.call(page_element)
        end

      end
      scopes.pop
    end

  end
end