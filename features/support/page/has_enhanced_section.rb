
# NOTE: probably should just forked site prism rather than doing all this. If I get time then
# I'd just abstract out the create_section method to lookup a section by name.
module SitePrism::ElementContainer

  private
  def extract_section_options(args, &block)
    if args.first.is_a? Class
      section_class = args.shift
    elsif block_given?
      section_class = Class.new Page::Section, &block
    else
      raise ArgumentError.new 'You should provide section class either as a block, or as the second argument'
    end
    return section_class, args
  end
end

module Page

  module HasEnhancedSection

    extend ActiveSupport::Concern

    # included do
    #   attr_accessor :selector
    # end

    def attr(name)
      self.root_element[name]
    end

    def get(name)
      self.wait_until_visible(name)
      self.send(name.gsub(' ', '_'))
    end

    def wait_until_invisible(name)
      self.send("wait_until_#{name.gsub(' ', '_')}_invisible")
    end

    def wait_until_visible(name)
      input = self.send(name.gsub(' ', '_'))
      puts input.inspect
      self.send("wait_until_#{name.gsub(' ', '_')}_visible")
    end

    # when invoked using execute_script it will return the jquery object for this sections root_element. Must be used in the
    # context of execute_script.  E.g. execute_script("#{jquery_element_expression}.find")
    def jquery_element_expression
      "$(document.evaluate('#{self.root_element.path}', document).iterateNext())"
    end

    def angular_scope_expression
      # this requires https://github.com/google/wicked-good-xpath to be in the page. This is
      # installed using the standard rails asset path.
      # Note: we're also getting the scope of the first child as in some cases (e.g.. sceneTableController)
      # the root element of the component is returning the parent scope instead of the component scope.
      "#{jquery_element_expression}.children().first().scope()"
    end

    def execute_script_with_scope(script)
      # this requires https://github.com/google/wicked-good-xpath to be in the page. This is
      # installed using the standard rails asset path.
      # puts angular_scope_expression
      self.execute_script("var scope = #{angular_scope_expression}; scope.$apply(function() {scope.#{script}});")
    end

    def evaluate_script_with_scope(script)
      # this requires https://github.com/google/wicked-good-xpath to be in the page. This is
      # installed using the standard rails asset path.
      self.evaluate_script("#{angular_scope_expression}.#{script}")
    end




    module ClassMethods

      include ::Page::HasWidgets

      def section(id, *find_args, &block)
        named = find_args.last.is_a?(Hash) ? find_args.last.delete(:as) : nil
        # super for class methods included by a `extend <module>`.... sigh.
        # Please note that you can't add this to the monkey patch above as there's no superclass method
        # to call (since you're redefining the existing one).
        self.singleton_class.included_modules.find { |m| m == ::SitePrism::ElementContainer }.instance_method(:section).bind(self).call(id, *find_args, &block)
      end

      def string_input(name, model_name: nil, selector: nil)
        input(name, 'Page::Material::StringInput', model_name: model_name, selector: selector)
      end

      def password_input(name, model_name: nil, selector: nil)
        input(name, 'Page::Material::StringInput', model_name: model_name, selector: selector)
      end

      def select_input(name, model_name: nil, selector: nil)
        input(name, 'Page::Material::SelectInput', model_name: model_name, selector: selector)
      end

      def radio_button_input(name, value: nil)
        input(name, 'Page::Material::RadioButtonInput', model_name: nil, selector: "radio-button-input[value=#{value}]")
      end

      def radio_group_input(name, model_name: nil, selector: nil)
        input(name, 'Page::Material::RadioGroupInput', model_name: model_name, selector: selector)
      end

      def number_input(name, model_name: nil, selector: nil)
        input(name, 'Page::Material::NumberInput', model_name: model_name, selector: selector)
      end

      def lux_input(name, model_name: nil, selector: nil)
        input(name, 'Page::Material::LuxInput', model_name: model_name, selector: selector)
      end

      def duration_input(name, model_name: nil, selector: nil)
        input(name, 'Page::Material::TimeDurationInput', model_name: model_name, selector: selector)
      end

      def slider_input(name, model_name: nil, selector: nil)
        input(name, 'Page::Material::SliderInput', model_name: model_name, selector: nil)
      end

      def checkbox_group_input(name, model_name: nil, selector: nil)
        input(name, 'Page::Material::CheckboxGroupInput', model_name: model_name, selector: selector)
      end

      def checkbox_input(name, model_name: nil, selector: nil)
        input(name, 'Page::Material::CheckboxInput', model_name: model_name, selector: selector)
      end

      def autocomplete_input(name, model_name: nil, selector: nil)
        input(name, 'Page::Material::AutocompleteInput', model_name: model_name, selector: selector)
      end

      def input(name, class_name, model_name: nil, selector: nil)
        unless selector
          selector = %Q([value-model$="#{model_name || name.to_s.camelize(:lower)}"])
        end
        section(name, class_name.constantize, selector)
      end

    end

  end

end