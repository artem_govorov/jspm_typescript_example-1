module JsonUtils


  def self.jsonize_key(k)
    k.to_s.camelize(:lower).to_sym
  end

  def self.dejsonize_key(k)
    k.to_s.underscore.to_sym
  end

  def self.jsonize_keys(value)
    self.convert_keys(value) {|k| self.jsonize_key(k) }
  end

  def self.dejsonize_keys(value)
    self.convert_keys(value) {|k| self.dejsonize_key(k) }
  end

  def self.dejsonize_params(params)
    params.transform_keys! { |k| self.dejsonize_key(k) }
    params.transform_values! do |k|
      convert_keys(k) {|k| self.dejsonize_key(k) }
    end
    params
  end

  def self.convert_keys(value, &method)
    case value
      when Array
        value.map { |v| convert_keys(v, &method) }
      when Hash
        Hash[value.map{|k,v| [method.call(k), convert_keys(v, &method)]}]
      else
        value
    end
  end

end