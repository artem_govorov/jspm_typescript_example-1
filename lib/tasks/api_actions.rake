namespace :api_actions do

  task generate_dao: :environment  do
    # desc 'Generates schema.json from the api action controllers.'


    dir = ApiActions.config.template_output_directory

    ApiActions.config.resource_definitions.each do |resource_def|
      ApiActions::Template::DaoTemplate.new(resource_def).write(dir)
    end

  end

end