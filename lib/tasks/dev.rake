namespace :dev do

  task :init do
    Rake::Task['bootstrap:db'].invoke
    Rake::Task['dev:seed'].invoke
  end

  task :migrate do
    Rake::Task['db:migrate'].invoke
    Rake::Task['db:test:prepare'].invoke
  end

  # task :seed do
    # Rake::Task['db:seed:users'].invoke
    # Rake::Task['db:seed:facility'].invoke
  # end

  namespace :debug do
    task db: :environment do
      puts Rails.configuration.database_configuration[ENV['RAILS_ENV']].inspect
      puts ActiveRecord::Base.configurations[ENV['RAILS_ENV']].inspect
    end
  end

end