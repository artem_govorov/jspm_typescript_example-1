namespace :bootstrap do

  task :db => :environment do
    if ActiveRecord::Migrator.get_all_versions.empty?
      Rake::Task['db:schema:load'].invoke
      Rake::Task['db:seed'].invoke
    else
      Rake::Task['db:migrate'].invoke
    end

  end

  task :debug_env do
    config = Rails.configuration.database_configuration[Rails.env]
    puts Rails.env
    puts config.inspect
  end

end