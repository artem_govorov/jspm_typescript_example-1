module ApiActions
  module Action

    class Base

      extend ActiveModel::Callbacks
      define_model_callbacks :execute

      class_attribute :_exceptions, instance_accessor: false
      class_attribute :_http_method
      class_attribute :_publish, instance_predicate: false
      self._exceptions = []

      def self.raises(*exceptions, **options)
        options.symbolize_keys!
        options.assert_valid_keys(:if)
        predicate = options[:if] || ->(_) { true }

        exceptions.each do |exception|
          # have to replace so we get a copy our parent class
          # raises the first time we add a new one.
          if exception.is_a?(Symbol) then
            exception_code = exception
            exception_class = "ApiActions::Errors::#{exception.to_s.classify}".constantize
          else
            exception_code = exception.name.demodulize.underscore.downcase.to_sym
            exception_class = exception
          end

          self._exceptions += [OpenStruct.new(code: exception_code, predicate: predicate)]
          self.instance_exec do

            define_method("raise_#{exception_code}!") do |payload = nil, &block|
              # make sure any raise if: .. predicates are satisfied.
              Lang.illegal_state! unless predicate.call(self.resource_definition)
              exception = exception_class.new(payload)
              # any calls to publish from here on should collect the messages
              # on the exception.
              @message_collector = exception
              # call the block if passed to the raise method..
              self.instance_exec(&block) if block
              # call the on_raise_some_exception if it exists.
              self.try("on_raise_#{exception_code}")
              # also call any handlers added to the controller using `on_raising_any(:some_exception)`
              global_callback = self.resource_definition.global_exception_callback(exception_code)
              self.instance_exec(&global_callback) if global_callback
              raise exception
            end
          end
        end
      end


      def self.exceptions_for(resource_definition)
        self._exceptions.select { |e| e.predicate.call(resource_definition) }.map { |e| e.code }
      end

      # Hmmm, ugly getter and setter in one...
      def self.http_method(method)
        self._http_method = method
      end

      def self.get_http_method
        self._http_method
      end

      def self.route_url(resource_definition, parent_id_token: nil)
        Lang.abstract_method!(self.name)
      end

      def self.publish(should_publish)
        self._publish = should_publish
      end

      def self.publish?
        self._publish
      end

      def self.collection?
        false
      end


      attr_reader :action_definition, :resource_definition, :current_user, :controller

      delegate :name, to: :action_definition

      delegate :resource_name, :resource_class, :parent_class,
               :belongs_to?, :attribute?,
               :topic, :has_topic?,
               to: :resource_definition


      def initialize(action_definition, controller)
        @action_definition = action_definition
        @resource_definition = action_definition.resource_definition
        @controller = controller
      end

      def params
        @params
      end

      def header(key)
        @headers[key]
      end

      def resource_params
        self.require_params(params).permit(permitted_params)
      end

      def require_params(params)
        params
      end

      def permitted_params
        []
      end

      def execute(response, params, headers, current_user)
        @response = Assert.present!(response)
        @params = Assert.present!(params)
        @headers = Assert.present!(headers)
        @current_user = current_user
        @message_collector = @response
        run_callbacks(:execute) do
          on_execute
        end
        unless responded?
          raise ::ApiActions::Errors::InternalServerError.new("#{self.class.name} did not generate a response, you need to call respond_with or #{self.class.exceptions_for(resource_definition).map { |e| "raise_#{e}" }}")
        end
      end

      def on_execute
        Lang.abstract_method!
      end

      def responded?
        !@response.status.nil?
      end

      def respond_with(resource, status: :ok)
        # Assert.is_a!(message_or_messages, RequestResponse)
        # Note we could add a serializers here so actions to provide their own implementation.
        @response.status = status
        @response.resource = resource
      end

      def respond_with_nothing(status: :ok)
        @response.status = status # if we use :no_content here any messages won't be sent.
        @response.resource = nil
      end

      def meta(key, value)
        @response.add_meta(key, value)
      end

      def publish(message)
        @message_collector.add_message(message)
      end

      def publish_action_message(resource, **options)
        self.publish(create_action_message(resource, **options)) if self.publish?
      end

      def create_action_message(resource, **options)
        self.resource_definition.topic(self.name).create_message(resource, **options)
      end

      # "runtime" errors, can still be raised but catching them isn't required on the client.
      def raise_bad_request!(message)
        raise ApiActions::Errors::BadRequest.new(message)
      end

      def raise_optimistic_lock_token_missing!
        raise ApiActions::Errors::OptimisticLockTokenMissing.new
      end

      def publish?
        self._publish
      end

      private

    end


  end
end