module ApiActions
  module Action

      class Index < ApiActions::Action::CollectionAction

        include Logging::Loggable

        class_attribute :scopes, instance_writer: false
        class_attribute :default_scope, instance_writer: false

        http_method :get

        def self.define_scopes(*scopes, default:)
          self.default_scope = Assert.present!(default).to_s
          self.scopes = (scopes + [default]).uniq.map(&:to_s)
        end

        def self.collection?
          true
        end

        def on_execute
          respond_with(load_resources)
        end

        # Returns the allowed parameters for searching
        # Override this method in each API controller
        # to permit additional parameters to search on
        # @return [Hash]
        def query_params
          {}
        end

        # Returns the allowed parameters for pagination
        # @return [Hash]
        def page_params
          params.permit(:page, :page_size)
        end

        def where_clause(query_params)
          query_params
        end

        def load_resources
          resources = scope.where(query_params)
          meta(:total_resources, resources.count)
          if page_params[:page]
            resources = resources.page(page_params[:page]).per(page_params[:page_size])
          end
          # logger.info("loaded #{resource_class} -> got #{resources.count}")
          resources
        end

        def base_scope
          resource_definition.belongs_to? ? parent_resource.send(resource_definition.inverse_of) : resource_class
        end

        def scope

          return self.base_scope unless self.scopes?

          scope_name = params.permit(:scope)['scope'] || self.default_scope

          unless self.scopes.include?(scope_name)
            raise_bad_request!("Illegal scope parameter. Should be one of #{self.scopes.map(&:to_s).join(',')}, got #{scope_name}")
          end

          base_scope.send(scope_name)

        end

      end

  end
end