module ApiActions
  module Action


    class Show < ApiActions::Action::MemberAction

      http_method :get

      def on_execute
        respond_with(resource)
      end

    end

  end
end