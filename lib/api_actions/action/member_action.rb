module ApiActions
  module Action

      class MemberAction < ApiActions::Action::Base

        def self.route_url(resource_definition, parent_id_token: nil)
          resource_definition.member_url(parent_id_token: parent_id_token)
        end


        raises :resource_not_found, if: ->(resource_def) { !resource_def.attribute? }
        raises :parent_resource_not_found, if: ->(resource_def) { resource_def.attribute? }

        before_execute :load_resource!

        def resource
          @resource
        end

        def set_resource(resource)
          @resource = resource
        end

        # Use callbacks to share common setup or constraints between actions.
        def load_resource
          # todo, make this a strategy: self.load_strategy.load
          if self.resource_definition.attribute?
            raise_parent_resource_not_found! unless self.parent_resource
            self.parent_resource.send(self.resource_definition.resource_name)
          else
            self.resource_class.find_by(id: member_id_param)
          end
        end

        def member_id_param
          params[:id]
        end

        def load_resource!
          set_resource(load_resource)
          raise_resource_not_found! unless self.resource
        end

        def parent_resource
          raise 'Parent only allowed for when belongs_to has been specified for the controller' unless resource_definition.belongs_to?
          @parent_resource ||= if self.resource_definition.attribute?
                                 self.parent_class.find_by(id: params[resource_definition.parent_param_id])
                               else
                                 self.resource.send(resource_definition.resource_parent_attribute)
                               end
        end

        def on_raise_resource_not_found
          publish(topic(:invalidate_all).create_message(nil)) if has_topic?(:invalidate_all)
        end

        def on_raise_parent_resource_not_found
          # todo: if we had top level ResourceDefinitions rather than controllers we could handle this automatically.
        end

      end

    end
end