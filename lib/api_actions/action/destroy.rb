module ApiActions
  module Action

      class Destroy < ApiActions::Action::MutateBase

        http_method :delete

        def on_execute
          destroy_resource(resource)
          respond_with(resource)
          publish_action_message(resource)
        end

        def destroy_resource(resource)
          resource.destroy
        end

      end

  end
end