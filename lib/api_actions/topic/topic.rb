module ApiActions
  module Topic

    class Topic

      class_attribute :_valid_options
      self._valid_options = []

      def self.valid_options(*options)
        self._valid_options += options.map(&:to_sym)
      end

      attr_reader :name, :resource_def

      delegate :belongs_to?, to: :resource_def

      def initialize(name, resource_def)
        @name = name
        @resource_def = resource_def
      end

      def create_message(payload, **options)
        options = options.with_indifferent_access
        self.validate_options!(options)
        ApiActions::Message.new(self.broadcast_channel_ids_for(payload, options), self.name, self.serialize_payload(payload, options))
      end

      def payload_class
        Lang.abstract_method!
      end

      def serialize_payload(payload, options)
        Lang.abstract_method!
      end

      def broadcast_channel_ids_for(payload, options)
        Lang.abstract_method!
      end

      def channel_id(parent_token_id: nil)
        Lang.abstract_method!
      end

      def validate_options!(options)
        Lang.illegal_state!("This topic doesn't support options") if options.any? && self._valid_options.empty?

        options.keys.map(&:to_sym).each do |key|
          raise Lang.illegal_state!("#{key} is not a valid option, must be one of #{self._valid_options.map(&:to_s)}") unless self._valid_options.include?(key)
        end
      end


    end

  end
end