module ApiActions
  module Topic

    # only here to we can go define_topic :create without specifying the topic class.
    class CreateTopic < ::ApiActions::Topic::ResourceActionTopic
    end

  end
end