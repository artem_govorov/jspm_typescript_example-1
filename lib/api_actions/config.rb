module ApiActions
  class Config

    include Logging::Loggable

    attr_accessor :controller_module, :schema_generation_file, :template_output_directory

    def register_resource(resource_definition)
      @resource_definition_map[resource_definition.schema_key] = resource_definition
    end

    def resource_definition_for(schema_key)
      @resource_definition_map[schema_key]
    end

    def resource_definitions
      @resource_definition_map.values
    end

    def inject_routes(router_dsl)
      self.load_definitions
      self.resource_definitions.each do |resource_def|
        resource_def.inject_routes(router_dsl)
      end
    end

    def generate_schema
      self.resource_definitions.map { |definition| [definition.schema_key, definition.get_schema] }.to_h
    end

    def validate!
    end

    def load_definitions
      @resource_definition_map = {}
      controller_root = "#{Rails.root}/app/controllers"
      search_path = File.join("#{controller_root}/#{self.controller_module.underscore}", '**/*_controller.rb')

      Dir[search_path].sort_by { |path| path.split('/').length }.each do |f|
        # turns:
        #   "/Users/andrew/coolon/light_architect_we/app/controllers/api/resource_controller.rb"
        # into:
        #   "api/resource_controller"
        # then:
        #    "Api::ResourceController"
        #
        fully_qualified_class_name = f[1+controller_root.length..-4].camelize

        # Load the class.  This will auto load it on the first run and then reload it
        # on subsequent runs in the dev environment.
        controller_class = fully_qualified_class_name.constantize

        controller_class.resource_definitions.each { |resource_definition| self.register_resource(resource_definition) }

      end
    end


  end
end