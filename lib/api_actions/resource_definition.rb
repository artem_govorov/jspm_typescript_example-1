module ApiActions

  class ResourceDefinition < ResourceDefinitionBase

    module Dsl

      class Base
        def initialize(context)
          @context = context
        end

        def define_action(name, action_handler_class, role:)
          @context.create_action_definition(name, action_handler_class, role)
        end

        def define_topic(name, topic_class = nil)
          topic_class = "ApiActions::Topic::#{name.to_s.classify}Topic".constantize unless topic_class
          @context.create_topic(name, topic_class)
        end

      end

      class ActionDsl < Base

        def index(action_class=nil, role:)
          define_action(:index, action_class || ApiActions::Action::Index, role: role)
        end

        def show(action_class=nil, role:)
          define_action :show, action_class || ApiActions::Action::Show, role: role
        end

        def create(action_class=nil, role:)
          action_def = define_action :create, action_class || ApiActions::Action::Create, role: role
          define_topic(:create) if action_def.publish?
        end

        def update(action_class=nil, role:)
          action_def = define_action :update, action_class || ApiActions::Action::Update, role: role
          define_topic(:update) if action_def.publish?
        end

        def destroy(action_class=nil, role:)
          action_def = define_action :destroy, action_class || ApiActions::Action::Destroy, role: role
          define_topic(:destroy) if action_def.publish?
          # once resources can be deleted it's possible to get resource not found exceptions.  We
          # auto define the invalidate all topic so any member actions can will automatically send
          # invalidate_all to the client that made the request.
          define_topic(:invalidate_all) if action_def.publish?
        end

        def on_raising_any(*symbol_or_exception, &block)
          Array.wrap(symbol_or_exception).each do |exception_code_or_class|
            @context.add_global_exception_callback(exception_code_or_class, block)
          end
        end

      end

      class Builder < ActionDsl

        def resource(options = {})
          config = ResourceConfig.create(options)
          @context.instance_variable_set('@resource_config', config)
        end

        # Marks this controller as belonging to a parent resource.  When this is the case the controller
        # will associate the resource with r's parent scope on index, show and create.  Update and delete methods
        # are applied to the resource directly.
        def belongs_to(name, options = {})
          config = BelongsToConfig.create(name, options)
          @context.instance_variable_set('@belongs_to_config', config)
        end

      end

    end


    # Defines the global resource equivalent for belongs to resources.  Currently this is only
    # used for the index_all action and invalidate topics and hasn't have any thought past that.  ResourceDefinitions
    # that have global collections will generate messages with two channel ids, one for the nested
    # path and one for the root path. Invalidate only sends to the global collection (as it's often fired
    # when the belongs to relationship can't be determined, i.e. all we know is that the client is out of sync
    # and we need to force it to reload.)
    class BelongsToGlobalResourceDefinition < ResourceDefinitionBase

      class ActionDsl < Dsl::Base
        def index(action_class=nil, role:)
          define_action(:index, action_class || ApiActions::Action::Index, role: role)
        end
      end

      def initialize(parent)
        super(parent.controller, resource_config: parent.resource_config)
        @parent = parent
        @topics = parent.topics.clone(self)
      end

      def topics
        @topics
      end

      def controller_method_name_for(action_def)
        "#{action_def.name}_root"
      end

      def dsl
        ActionDsl.new(self)
      end

    end

    # Defines a nested resource that is retrieved using a method on the parent controller
    # rather than a direct DB query.  Actions need to check resource_def.attribute? if
    # they operated on attribute and entity resources.
    class NestedAttributeResourceDefinition < ResourceDefinitionBase

      def initialize(name, parent, options = {})
        super(parent.controller,
              resource_config: ResourceConfig.create(options.merge({named: name, attribute: true})),
              belongs_to_config: BelongsToConfig.create(parent.resource_name,
                                                        class: parent.resource_class,
                                                        inverse_of: name,
                                                        identified_by: parent.identified_by)
        )
        @parent = parent
      end


      def controller_method_name_for(action_def)
        "#{action_def.name}_#{resource_name}"
      end

      def dsl
        Dsl::ActionDsl.new(self)
      end

      def has_global_collection?
        false
      end

    end


    #
    # ResourceDefinition instance methods.
    #

    def initialize(controller)
      super(controller)
      @nested_definitions = {}.with_indifferent_access
    end

    def dsl
      Dsl::ActionDsl.new(self)
    end

    def builder
      Dsl::Builder.new(self)
    end

    def define_nested_collection_resource(name, options, &block)
      resource_definition = self.create_attribute_resource(name, options)
      resource_definition.dsl.instance_exec &block
    end

    def define_attribute_resource(name, options, &block)
      context = create_attribute_resource(name, options, has_one: true)
      context.dsl.instance_exec &block
    end

    def attribute_resource_definition(name)
      @nested_definitions[name]
    end

    def flatten
      all = [self] + @nested_definitions.values
      all << @global_resource if @global_resource
      all
    end

    # See https://docs.google.com/spreadsheets/d/1ebkfPI29RnzjMS37ip2hJ9cPvz2USIYLTLBGIBZGH-I/edit#gid=0
    # for the list of controller types.
    def has_global_collection?
      self.collection? && self.belongs_to? && !(self.attribute? || self.virtual?)
    end

    def global_resource_definition
      ensure_global_collection!
      @global_resource_definition ||= BelongsToGlobalResourceDefinition.new(self)
    end

    def ensure_global_collection!
      Lang.illegal_state!("can only create global resource for belongs to collection resources that aren't virtual or attribute based") unless self.has_global_collection?
    end

    # Returns the root resource definition for this resource.  This will
    def as_global_resource
      # remember someone has asked for a root resource, this is typically because a controller
      # has added an index all, so in that case we need a root controller.  This also means that
      # the definition needs to be included in the call to flatten (so we keep a separate varialbe for it.)
      # Fixme: Would it be better if the controller kept track of the definitions it creates, that way would wouldn't even need the flatten method...?
      ensure_global_collection!
      @global_resource ||= global_resource_definition
    end

    protected

    def create_attribute_resource(name, options, has_one: false)
      Lang.illegal_state!("attribute resource already defined for #{name}") if attribute_resource_definition(name)

      context = NestedAttributeResourceDefinition.new(name, self, options.merge(has_one: has_one))
      @nested_definitions[name] = context
    end

    def ensure_configured!
      Assert.present!(self.resource_name, "resource_name required for #{self.controller.name}")
      Assert.truthy!(self.parent_class, "belongs_to required for attribute resources for #{self.controller.name}") if self.attribute?
    end


  end

end