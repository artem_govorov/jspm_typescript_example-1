module ApiActions

  class BelongsToConfig
    BELONGS_TO_KEYS = [:as, :inverse_of, :class, :class_name, :identified_by]

    def self.create(name, options = {})
      options.symbolize_keys!
      options.assert_valid_keys(*BELONGS_TO_KEYS)
      name = name.to_s
      self.new.tap do |config|
        config.parent_name = name
        config.parent_class = options.delete(:class) || (options.delete(:class_name) || name.classify).constantize
        config.parent_accessor = options.delete(:as) || name
        config.inverse_of = options.delete(:inverse_of)
        config.identified_by = (options.include?(:identified_by) ? options.delete(:identified_by) : :id)
      end
    end

    attr_accessor :parent_name,
                  :parent_class,
                  :parent_accessor,
                  :inverse_of,
                  :identified_by
  end

end