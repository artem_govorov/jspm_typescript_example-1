module ApiActions::Template

  class SaveRequestTemplate

    CLASS_TEMPLATE = <<-eos
  export class <%= class_name %> extends AbstractDelegatingRequest<<%= return_type %>> {

    private _request:AbstractRequest;

    get errorCodes():[string] { return [<%= exception_code_array %>]; }

    constructor(request:AbstractRequest<<%= return_type %>>) {
      super(request);
    }
    <% all_exception_codes.each do |c| %>
    catch<%= c.camelize %>(callback:ErrorCallback):<%= namespace %>.<%= class_name %> {
      this._registeredErrorHandlers.push('<%= c.camelize(:lower) %>');
      if (this._request['catch<%= c.camelize %>']) { this._request['catch<%= c.camelize %>'](callback) }
      return this;
    }<% end %><% all_exception_codes.each do |c| %>
    spyOn<%= c.camelize %>(callback:ErrorCallback):<%= namespace %>.<%= class_name %> {
      if (this._request['spyOn<%= c.camelize %>']) { this._request['spyOn<%= c.camelize %>'](callback) }
      return this;
    }<% end %>
  }
    eos

    attr_reader :create_def, :update_def, :namespace, :type, :parent_type

    def initialize(create_action_def, update_action_def, namespace, type, parent_type)
      @create_def = create_action_def
      @update_def = update_action_def
      @namespace = namespace
      @type = type
      @parent_type = parent_type
    end

    def class_name
      'Save'
    end

    def return_type
      create_def.collection? ? "[#{type}]" : type
    end

    def all_exceptions
      (create_def.exceptions + update_def.exceptions).uniq
    end

    def all_exception_codes
      all_exceptions.map { |n| n.to_s.camelize(:lower) }
    end

    def exception_code_array
      all_exception_codes.map { |c| "'#{c}'" }.join(',')
    end

    def action_handler_name
      'save'
    end

    def permitted_params
      (create_def.create_handler.permitted_params + update_def.create_handler.permitted_params).uniq..map { |n| n.to_s.camelize(:lower) }
    end

    def phantom?
      create_def.resource_definition.phantom?
    end

    def parameter_type
      # this needs to handle all DAO's such as the rule modification ones (deleteOne, deleteRemaining etc).
      # params = []
      # params << 'id' if action_def.member_action?
      # params += permitted_params
      # "{#{params.map { |p| "#{p}?:any" }.join(',')}}"
      'any'
    end

    def render_request_class
      ERB.new(CLASS_TEMPLATE).result(binding)
    end

    def render_action_method
      ERB.new(DAO_METHOD).result(binding)
    end

  end

end