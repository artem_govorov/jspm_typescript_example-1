module ApiActions
  module Errors
    class ValidationError < Base
      def initialize(payload = {})
        super(422, payload)
      end
    end
  end
end