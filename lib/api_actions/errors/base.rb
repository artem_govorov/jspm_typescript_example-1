module ApiActions
  module Errors

    class Base < StandardError

      attr_reader :http_response_code, :payload

      def initialize(http_response_code, payload)
        @http_response_code = http_response_code
        @payload = payload
      end

      def messages
        @messages ||= []
      end

      def add_message(message)
        self.messages << message
      end

      def code
        self.class.name.demodulize.camelize(:lower)
      end

      def as_json
        JsonFormat.transform_hash_keys({code: self.code, payload: self.payload.as_json})
      end

    end
  end
end