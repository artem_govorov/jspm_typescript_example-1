module ApiActions
  module Errors
    class OptimisticLockTokenMissing < Base
      def initialize
        super(428, {})
      end
    end
  end
end