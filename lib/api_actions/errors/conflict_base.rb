module ApiActions
  module Errors

    class ConflictBase < Base
      def initialize(data = {})
        super(409, data)
      end
    end

  end
end