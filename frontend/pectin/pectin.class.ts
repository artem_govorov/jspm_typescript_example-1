import * as _ from 'lodash';
import {lang, assert} from './lang';
import {Tuple} from "./model/tuple.class";
import IDeferred = angular.IDeferred;

export class Pectin {

    private promiseImpl;
    private _defaultTupleType;

    // private promiseImpl:any;
    // private _defaultTupleType:any;

    constructor() {
        if (Pectin['__instance']) {
            throw new Error("Error - use Pectin.get()");
        }
        this._defaultTupleType = Tuple;
    }

    static get(): Pectin {
        Pectin['__instance'] = Pectin['__instance'] || new Pectin();
        return Pectin['__instance'];
    }

    static when(objectOrPromise) {
        return Pectin.get().when(objectOrPromise);
    }

    static reject(reason) {
        return Pectin.get().reject(reason);
    }

    static all(...objectOrPromise) {
        return Pectin.get().all(objectOrPromise);
    }

    static defer<T>():IDeferred<T> {
        return Pectin.get().defer();
    }

    setPromiseImpl(promiseImpl) {
        this.promiseImpl = assert.notNull(promiseImpl);
    }
    
    setDefaultTupleType(defaultTupleType:(left:any, right:any) => Tuple<any>) {
        this._defaultTupleType = defaultTupleType;
    }
    
    get defaultTupleType() {
        return this._defaultTupleType;
    }
    
    resolve(objectOrPromise) {
        return this.when(objectOrPromise);
    }

    all(...objectsOrPromiseArrays) {
        this._ensureConfigured();
        return this.promiseImpl.all(_.flattenDeep(objectsOrPromiseArrays));
    }

    when(objectOrPromise) {
        this._ensureConfigured();
        return this.promiseImpl.when(objectOrPromise);
    }

    reject(reason) {
        this._ensureConfigured();
        return this.promiseImpl.reject(reason);
    }

    defer<T>():IDeferred<T> {
        return this.promiseImpl.defer();
    }

    _ensureConfigured() {
        if (this.promiseImpl === null || this.promiseImpl === undefined) {
            lang.illegalState('must configure promise api using Pectin.setPromiseImpl(...)');
        }
    }
}