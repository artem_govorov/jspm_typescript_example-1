//noinspection TypeScriptCheckImport
import inspect from 'object-inspect';
import chalk from 'chalk';

import {DebugContext} from "./debugContext.class";

let traceDepth = 0;

function tracePrefix() {
    return ' '.repeat(traceDepth * 3);
}

export class Trace {

    static log(message: string, debug: any = null) {
        console.log(`${tracePrefix()}${message}`)
    }

    static wrapMethod(f: Function, methodName, target, predicate: (_this: any)=>boolean = ()=>true): Function {
        const namedArgs = Trace.getArgs(f);
        return function (...args) {
            if (predicate(this)) {
                const context = DebugContext.get(this).toString();
                const indent = tracePrefix();
                try {
                    console.log(`${indent}>  ${context}.${methodName} [in ${target.constructor.name}] {`);
                    namedArgs.forEach((name, index) => {
                        console.log(chalk.gray(`${indent}      ${name}=${index < args.length? DebugContext.inspect(args[index]) : 'missing!!'}`));
                    });
                    // todo add missing args.
                    traceDepth++;
                    const result = f.call(this, ...args);
                    console.log(`${indent}   }` + chalk.gray(`:${DebugContext.inspect(result)}`));
                    return result;
                } catch (err) {
                    console.log(chalk.red(`${indent}!!! ${context}.${methodName}: ${DebugContext.inspect(err)}`));
                    throw err;
                } finally {
                    traceDepth--;
                }
            } else {
                return f.call(this, ...args);
            }
        };
    }

    static getArgs(func) {
        // First match everything inside the function argument parens.
        var args = func.toString().match(/function\s.*?\(([^)]*)\)/)[1];

        // Split the arguments string into an array comma delimited.
        return args.split(',').map(function (arg) {
            // Ensure no inline comments are parsed and trim the whitespace.
            return arg.replace(/\/\*.*\*\//, '').trim();
        }).filter(function (arg) {
            // Ensure no undefined values are added.
            return arg;
        });
    }
}

