//noinspection TypeScriptCheckImport
import inspect from 'object-inspect';

import * as _ from 'lodash';
import {assert, ConstructorFunction} from "../lang";
import {Trace} from "./trace.class";
import {DebugContext} from "./debugContext.class";

export function debugContext(prototype, property) {

    Object.defineProperty(prototype, property, {
        get: function () {
            return this[`__${property}`]
        },
        set: function (value) {
            //todo, move this into the DebugContext class.
            const currentValue = this[`__${property}`];
            DebugContext.remove(currentValue);
            this[`__${property}`] = value;
            DebugContext.inject(value, property, this);
        },
    });
}

export function trace(target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    assert.isFalsey(_.isFunction(target)); // only handling instance methods for now.
    descriptor.value = Trace.wrapMethod(descriptor.value, propertyKey, target);
    return descriptor;
}

export function traceIfDebugContext(target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    assert.isFalsey(_.isFunction(target)); // only handling instance methods for now.
    descriptor.value = Trace.wrapMethod(descriptor.value, propertyKey, target, (_this) => DebugContext.get(_this));
    return descriptor;
}


export function DebugString(...properties:string[]) {
    return function(targetClass:ConstructorFunction<any>)  {
        targetClass.prototype['toDebugString'] = function() {
            return '' + (properties.length > 0 ? `[${properties.map(p => `${p}=${DebugContext.inspect(this[p])}`).join(',')}]` : inspect(this))
        }
    }
}



