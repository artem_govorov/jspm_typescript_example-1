import * as _ from 'lodash';
import S from 'string';
import {assert,lang} from '../lang';

export function CommandDirective($compile, $parse:angular.ng.IParseService, hotkeys) {
    return {
        restrict: 'A',
        terminal: true, //this setting is important, stops angular processing children automatically.
        priority: 1000, //make sure we run before any other directives.
        compile: function compile(tElement:angular.ng.IAugmentedJQuery, tAttrs:angular.ng.IAttributes) {

            // add a do nothing href so links styles work as expected
            if (tElement[0].nodeName.toLowerCase() == 'a') {
                tElement.attr('href', '')
            }

            tElement.removeAttr("command"); //remove ourselves so we don't recurse when compiling.

            let commandPath = tAttrs.command;
            let commandExpr = $parse(commandPath);
            let commandId = tAttrs.commandId || S(_.last(commandPath.split('.'))).dasherize().s;

            let param = tElement.attr('command-parameter');

            if (param) {
                tElement.attr('ng-click', `${commandPath}.execute(${param}, {event: $event})`);
            } else {
                tElement.attr('ng-click', `${commandPath}.execute(null, {event: $event})`);
            }

            const shortcutText = tElement.attr('shortcut');
            let shortcutExpr = $parse(shortcutText);
            const shortcutDescriptionText = tElement.attr('description');
            // let shortcutDescriptionExpr = $parse(shortcutDescriptionText);

            tElement.attr('ng-disabled', `${commandPath}.disabled`);

            let busyTextElem = tElement.find('busy-text');
            if (busyTextElem.length) {
                let normalText = tElement.find('text');
                if (!normalText.length) {
                    lang.illegalState('commands with busy-text element must also have a text element')
                }
                normalText.attr('ng-hide', `${commandPath}.busy`);
                busyTextElem.attr('ng-show', `${commandPath}.busy`);
            }

            let transclude = $compile(tElement);

            return {
                pre: function preLink(scope, iElement, iAttrs, controller) {
                },
                post: function postLink(scope, iElement, iAttrs, controller) {

                    transclude(scope, function (clone, scope) {
                        let command = assert.notNull(commandExpr(scope), `${commandPath}`);
                        let shortcut = (shortcutExpr(scope) || shortcutText);
                        if (shortcut) {
                            hotkeys.bindTo(scope).add({
                                combo: shortcut,
                                description: shortcutDescriptionText,
                                callback: (param) => command.execute(param)
                            });
                        }
                        clone.attr('command-id', commandId);
                        iElement.replaceWith(clone);
                    });
                }
            };
        }
    }
}
