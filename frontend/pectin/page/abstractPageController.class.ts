import {valueModel, valueOf} from "../binding/dsl";
import {UiCommand} from "../command/uiCommand.class";
import {Disposable} from "../binding/disposable.class";
import {assert, lang} from "../lang";
import {Pectin} from "../pectin.class";
import {Binder} from "../binding/binder.class";
import IPromise = angular.IPromise;
import {PageController} from "./pageController.class";
import {ValueHolder, ValueModel} from "../model/baseValueModels";
import {ListModel} from "../model/baseListModels";
import IDeferred = angular.IDeferred;

class ChangePageCommand extends UiCommand<void,void> {

    constructor(protected controller:AbstractPageController, protected direction:number) {
        super();
    }

    onExecute(ignore, context) {
        if (this['enabled']) {
            this.controller.gotoPage(this.controller.pageNumber + this.direction);
        }
    }
}

class NextCommand extends ChangePageCommand {
    constructor(controller) {
        super(controller, 1);
        this['enableWhen'](valueOf(this.controller.pageNumberModel).isLessThan(this.controller.totalPagesModel))
    }
}

class PreviousCommand extends ChangePageCommand {
    constructor(controller:AbstractPageController) {
        super(controller, -1);
        this['enableWhen'](valueOf(this.controller.pageNumberModel).isGreaterThan(1))
    }
}

export interface PageResult<T> {
    items:T[];
    totalItems:number;
}

export abstract class AbstractPageController<T> extends Disposable implements PageController<T> {

    private _pageSizeModel: ValueHolder<number> = new ValueHolder(-1);
    protected _totalItemsModel: ValueModel<number>;

    protected _pageNumberModel: ValueHolder<number> = new ValueHolder(1);
    protected _totalPagesModel: ValueModel<number> = new ValueHolder(0);

    private _busyModel:ValueModel<boolean> = new ValueHolder(false);

    private _nextCommand: UiCommand;
    private _previousCommand: UiCommand;

    private _deferredInitialize:IDeferred<PageController> = Pectin.defer();
    private _initialized:boolean = false;

    constructor() {
        super();
        this._totalItemsModel = new ValueHolder<number>(0);
        this._totalPagesModel = valueModel.computedFrom(this._totalItemsModel).using((count) => {
            const pageSize = this._pageSizeModel.value;
            return (count > 0 && pageSize > 0) ? Math.ceil(count / pageSize) : 1;
        });
        this._totalPagesModel['dependsOn'](this._pageSizeModel);

        this._nextCommand = new NextCommand(this);
        this._previousCommand = new PreviousCommand(this);
    }

    protected abstract doFirstLoad():IPromise<any>;
    protected abstract fetchPage(pageIndex, pageSize):IPromise<PageResult<T>>|PageResult<T>
    protected abstract get currentPageModel(): ListModel<T>;
    protected abstract setCurrentPageItems(items:T[]):void;

    public gotoPage(pageNumber): IPromise<void> {
        const pageSize = this._pageSizeModel.value;
        assert.isTruthy(pageSize > 0, "can't load page until page size is set.");
        assert.isInteger(pageNumber);
        assert.isTruthy(pageNumber > 0, 'pageNumber must be greater that 0');
        let pageIndex = pageNumber - 1;
        this._busyModel.value = true;
        return Pectin.when(this.fetchPage(pageIndex, pageSize)).then((result:PageResult<T>) => {
            this.setCurrentPageItems(result.items);
            this.updateTotalItems(result.totalItems);
            this._pageNumberModel.value = Math.min(pageNumber, this.totalPages) ;
        }).finally(() => {
            this._busyModel.value = false;
        });
    }

    resetToFirstPage() {
        if (this.isInitialized()) {
            this.gotoPage(1);
        }
    }

    protected updateTotalItems(count) {
        this._totalItemsModel.value = count;
    }

    public reloadCurrentPage() {
        return this.gotoPage(this.pageNumber);
    }

    setPageSize(size:number) {
        if (size < 1) {
            lang.illegalState(`page size must be greater than 0, got: ${size}`)
        }
        const firstTime = !this.isInitialized();
        const oldPageSize = this._pageSizeModel.value;
        this._pageSizeModel.value = size;
        if (firstTime) {
            // we've never been loaded so we goto page 1.
            this._busyModel.value = true;
            this.doFirstLoad().then(() => {
                this._initialized = true;
                this._busyModel.value = false;
                this._deferredInitialize.resolve(this);
            });
        } else {
            // otherwise we try and stay close to the first item on the old page in the
            // new page structure.
            if (size > 0 && (size != oldPageSize)) {
                let firstItemSourceIndex = (this._pageNumberModel.value - 1) * oldPageSize;
                const newIndex = Math.floor(firstItemSourceIndex / size);
                this.gotoPage(1 + newIndex);
            }
        }
    }

    isInitialized() {
        return this._initialized;
    }

    waitUntilInitialized(): angular.IPromise<PageController<T>> {
        return this._deferredInitialize.promise;
    }

    get pageNumberModel():ValueModel<number> {
        return this._pageNumberModel;
    }

    get pageNumber() {
        return this.pageNumberModel.value;
    }

    get pageSizeModel():ValueModel<number> {
        return this._pageSizeModel.immutable();
    }

    get pageSize():number {
        return this.pageSizeModel.value;
    }

    get totalPagesModel():ValueModel<number> {
        return this._totalPagesModel.immutable();
    }

    get totalPages():number {
        return this.totalPagesModel.value;
    }

    get busyModel():ValueModel<boolean> {
        return this._busyModel.immutable();
    }

    get busy():boolean {
        return this._busyModel.value;
    }

    get nextCommand():UiCommand {
        return this._nextCommand;
    }

    get previousCommand():UiCommand {
        return this._previousCommand;
    }

}