//noinspection TypeScriptCheckImport
import inspect from 'object-inspect';
import {ListModel, ListHolder} from "../models";
import {AbstractPageController, PageResult} from "./abstractPageController.class";
import {assert} from "../lang";

class PagedListModel<T> extends ListHolder<T> {

    public readonly pageController;

    constructor(pager:ListModelPager) {
        super();
        this.pageController = pager;
    }

}
export class ListModelPager<T> extends AbstractPageController {

    private _source:ListModel<T>;
    private _pageView:PagedListModel<T>;

    constructor(source:ListModel<T>) {
        super();
        this._source = assert.notNull(source);
        this._pageView = new PagedListModel<T>(this);
        this._source.onValueChange((newValues:T[], oldValues:T[]) => {
            if (this.isInitialized()) {
                this.reloadCurrentPage();
            }
        });
    }

    doFirstLoad(): angular.IPromise<any> {
        return this.gotoPage(1);
    }

    fetchPage(pageIndex, pageSize):PageResult<T> {
        // console.log(`loading page ${1+pageIndex} of ${this.totalPages} (size=${pageSize})`);
        const startIndex = pageIndex * pageSize;
        const items = this._source.values.slice(startIndex, startIndex + pageSize);
        return {
            items: items,
            totalItems: this._source.length
        }
    }

    get currentPageModel(): ListModel<T> {
        return this._pageView;
    }

    protected setCurrentPageItems(items: T[]): void {
        // console.log(`--> setCurrentPageItems: ${inspect(items.map(i => i.id))}`);
        this._pageView.value = items;
    }
}