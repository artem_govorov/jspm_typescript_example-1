import {ValueModel} from "../models";
import {UiCommand} from "../command/uiCommand.class";
import {IDisposable} from "../binding/disposable.class";
import IPromise = angular.IPromise;
import {ListModel} from "../models";

export interface PageController<T> extends IDisposable {

    isInitialized(): boolean;
    setPageSize(size:number):void;
    gotoPage(pageNumber:number):IPromise<void>
    resetToFirstPage():void;
    waitUntilInitialized():IPromise<PageController<T>>
    readonly pageNumberModel: ValueModel<number>
    readonly pageNumber: number;
    readonly pageSizeModel: ValueModel<number>;
    readonly pageSize: number;
    readonly totalPagesModel: ValueModel<number>;
    readonly totalPages: number;
    readonly busyModel: ValueModel<boolean>;
    readonly busy: boolean;
    readonly nextCommand: UiCommand;
    readonly previousCommand: UiCommand;
    readonly currentPage:ListModel<T>;
}