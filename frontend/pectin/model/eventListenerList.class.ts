import * as _ from 'lodash';

import {assert} from '../lang';
import {Pectin} from 'pectin/pectin.class';
import {IDisposable, Disposable} from "../binding/disposable.class";

class ListRegistration<T> extends Disposable {

    listener:T;

    constructor(listener:T){
        super();
        this.listener = listener;
    }
}

export class EventListenerList<T> {

    private _registrations:Array<ListRegistration<T>> = [];

    /**
     * Registers a listener.
     * @param listener
     * @return [Disposable] returns a HandlerRegistration that can be used to deregister the listener and free
     * the memory reference to it.  e.g. registration.dispose();
     */
    add(listener:T):ListRegistration<T> {
        assert.notNull(listener);
        let registration = new ListRegistration(listener);
        this._registrations.push(registration);
        registration.onDispose(() => {
            let index = this._registrations.indexOf(registration);
            if (index > -1) {
                this._registrations.splice(index, 1);
                // console.log(`registration found`);
            } else {
                console.log(`dispose() -> registration not found!!`);
            }
        });
        return registration;
    }

    fire(invoker:(listener:T)=>any):Array<any> {
        return _.map(this._registrations, (registration) => invoker(registration.listener));
    }

    fireAndWait(invoker:(listener:T)=>any):angular.IPromise<Array<any>> {
        return Pectin.get().all(this.fire(invoker));
    }
}
