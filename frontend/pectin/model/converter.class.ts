export interface Converter<T,S> {
    fromSource(sourceValue:S):T;
    toSource(convertedValue:T):S;
}