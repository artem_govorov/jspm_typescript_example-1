import * as _ from 'lodash';
import {Mixin} from '../traits';
import {lang, assert} from '../lang';
import {AbstractHasValue, HasValue} from './abstractHasValue.class';
import {HasSourceModels, HasSourceModelsTrait} from './hasSourceModels.trait';
import {DelegatingModel} from './delegatingModel.decorator';
import {IDelegatingModel} from "./delegatingModel.decorator";
import {trace, traceIfDebugContext} from "../debug/debug.decorators";
import {DebugContext} from "../debug/debugContext.class";
import {ValueChangeTransaction} from "./hasValueChangeListeners.class";
import {Trace} from "../debug/trace.class";
import {IDisposable} from "../binding/disposable.class";
import {ModelBinding} from "../binding/modelBinding.class";
import {Converter} from "./converter.class";


// TODO: Make this happen.
// interface HasValue<T> {readonly value:T; onValueChange(...)}
// interface HasMutableValue<T> extends HasValue<T> { setValue(value:T) }
// interface ValueModel<T> extends HasValue<T> {withDefault()}
// interface MutableValueModel<T> extends ValueModel<T>
// interface ListModel<T> extends ValueModel<T[]>
// interface MutableListModel<T> extends ListModel<T>
// export interface MutableValueModel<T> extends HasValue<T> {
//     setValue(value:T);
// }

export class ValueModel<T> extends AbstractHasValue<T> {

    private _immutableModel:ValueModel<T>;

    withDefault(value:T):ValueModel<T> {
        var model = new DelegatingValueModel<T>(this);
        model.setDefault(value);
        return model;
    };

    immutable():ValueModel<T> {
        if (!this._immutableModel) {
            this._immutableModel = new ImmutableValueModel<T>(this);
        }
        return this._immutableModel;
    }
}

export class ValueHolder<T> extends ValueModel<T> { //implements MutableValueModel<T> {

    private _value:T;

    constructor(initialValue?:T) {
        super();
        this.value = initialValue;
    }

    doRead():T {
        return this._value;
    }

    doWrite(value:T) {
        this._value = value;
    }
}

const NEVER_COMPUTED = Object.freeze({});
// temp hack till the IDE is ok with the syntax
@Mixin(HasSourceModelsTrait)
export abstract class AbstractComputedValueModel<T> extends ValueModel<T> {

    protected _computedValue: T;

    constructor() {
        super();
        this._computedValue = NEVER_COMPUTED;
    }

    doRead(): T {
        // compute on first call if not prior.
        // This creates double computes when firing change events as the read of the previous
        // value triggers a compute, as does the actual handling of the event.
        if (this.valueHasNeverBeenComputed()) {
            this.commitTransaction();
        }
        return this._computedValue;
    }

    abstract doComputeValue(): T;

    onSourceValueChanged(newValue: any, oldValue: any, source: ValueModel<any>) {
        this.recompute();
    }

    protected commitTransaction() {
        this._computedValue = this.doComputeValue();
        return this.value;
    }

    getValuePriorToChange(): T {
        return this.valueHasNeverBeenComputed() ? null : this._computedValue;
    }

    valueHasNeverBeenComputed() {
        return this._computedValue === NEVER_COMPUTED;
    }

    recompute() {
        // this will trigger a transaction and a update to the computed value.
        this.fireValueChange();
    }

    /**
     * Fires a change to the list by replacing array with a copy of the current one.  This is
     * useful if you've changed and element of the list and want dependents to update or re-render.
     */
    fireChange() {
        this.recompute();
    }

}
//update our class definition with the mixin methods...
export interface AbstractComputedValueModel<T, S> extends HasSourceModels<S> {}

// temp hack till the IDE is ok with the syntax
export class ComputedValueModel<T,S> extends AbstractComputedValueModel<T, S> {

    private _source:ValueModel<S>;
    private _computeNulls:boolean  =false;

    constructor(source:ValueModel<S>, fn?:(value:S)=>T) {
        super();
        this._source = assert.notNull(source, 'source');
        this.dependsOn(source);
        if (fn) {
            this.compute = fn;
        }
    }

    get source():ValueModel<S> {
        return this._source;
    }

    computeWhenSourceOrValueIsNull():this {
        this._computeNulls = true;
        // force a recompute as our value will probably be different
        this.recompute();
        return this;
    }

    protected doComputeValue() {
        let value = this.source ? this.source.value : null;
        if (this._isDefined(value)) {
            return this.compute(value)
        } else {
            return this._computeNulls ? this.compute(null) : null;
        }
    }

    _isDefined(value) {
        return !_.isNull(value) && !_.isUndefined(value);
    }

    compute(value:S):T {
        return lang.abstractMethod(this.constructor)();
    }

}



export class ImmutableValueModel<T> extends ValueModel {
    private _source:ValueModel<T>;
    private _previousValue:T;

    constructor(source:ValueModel<T>) {
        super();
        this._source = source;
        this._source.onValueChange((newValue, oldValue) => {
            this._previousValue = oldValue;
            // fires a value change for us.
            this.fireValueChange();
        })
    }

    getValuePriorToChange(): T {
        return this._previousValue;
    }

    doRead():T {
        return this._source.value;
    }

    immutable(): ValueModel<T> {
        return this;
    }
}

@DelegatingModel
class AbstractDelegatingValueModel<T> extends AbstractComputedValueModel<T, T> {}
// @see https://github.com/Microsoft/TypeScript/issues/5627#issuecomment-155955170
export interface AbstractDelegatingValueModel<T> extends IDelegatingModel<T> {}

export class DelegatingValueModel<T> extends AbstractDelegatingValueModel<T> {
    constructor(delegate?:ValueModel<T>) {
        super();
        if (_.isObject(delegate) && !(delegate instanceof ValueModel)) {
            lang.illegalArgument('delegate must be a ValueModel');
        }
        this.setDelegate(delegate || new ValueHolder<T>());
    }

    // needed by AbstractComputedValueModel transaction finalization.
    updateComputedValue() {
        // noop
    }
}

// this is really a reducing... model
export class ReducingValueModel<T,S> extends AbstractComputedValueModel<T> {

    private _sources:[ValueModel<S>] = [];

    constructor(sources:ValueModel<S>[], fn?:(values:S[])=>T) {
        super();
        _.each(sources, source => this.addSource(source));
        if (fn) {
            this.compute = fn;
            this.updateComputedValue();
        }
    }

    addSource(source:ValueModel<S>):void {
        assert.notNil(source);
        this.fireValueChangeAfter(() => {
            this.dependsOn(source);
            this._sources.push(source);
        })
    }

    compute(values:S[]):T {
        return lang.abstractMethod(this.constructor)();
    }

    doComputeValue() {
        let values = _.map(this._sources, s => s.value);
        // force our result to be true or false.
        return this.compute(values)
    }
}

export class ConvertingValueModel<T,S> extends ValueHolder<T> implements Converter<T,S>, IDisposable {

    private binding:ModelBinding;
    private source:ValueModel<S>;

    constructor(source:ValueModel<T>, converter?:Converter<T,S>) {
        super();
        this.source = source;
        if (converter) {
            this.fromSource = converter.fromSource;
            this.toSource = converter.toSource;
        }
        this.binding = new ModelBinding(source, this, this).writeRight();
    }

    fromSource(sourceValue: S): T {
        return lang.abstractMethod('fromSource');
    }

    toSource(convertedValue: T): S {
        return lang.abstractMethod('toSource');
    }

    writeLeft() {
        this.binding.writeLeft();
    }

    writeRight() {
        this.binding.writeRight();
    }

    dispose(): void {
        this.binding.dispose()
    }
}

export class DebouncedValueModel<T> extends ValueModel<T,T> {

    private _source:ValueModel<T>;
    private _millisecondDelay:number;

    private _timeout:any;
    private _debouncedValue:T;

    constructor(source:ValueModel<T>, millisecondDelay:number = 250) {
        super();
        this._source = source;
        this._millisecondDelay = millisecondDelay;
        this._source.onValueChange(() => this.triggerUpdate());
        this.updateFromSource();
    }

    doRead():T {
        return this._debouncedValue;
    }

    triggerUpdate() {
        if (this._timeout) {
            clearTimeout(this._timeout);
        }
        this._timeout = setTimeout(() => this.updateFromSource(), this._millisecondDelay);
    }

    updateFromSource() {
        this.fireValueChangeAfter(() => {
            this._debouncedValue = this._source.value;
        });
    }
}

export class StickyValueModel<T> extends ComputedValueModel<T,T> {
    private _lastKnownValue:T;

    constructor(source:ValueModel<T>) {
        super(source);
        this.computeWhenSourceOrValueIsNull();
    }

    compute(value:T):T {
        if (this._isDefined(value)) {
            this._lastKnownValue = value;
            return value;
        }
        else {
            return this._lastKnownValue;
        }
    }

    _isDefined(value):boolean {
        return !_.isUndefined(value) && !_.isNull(value);
    }
}

