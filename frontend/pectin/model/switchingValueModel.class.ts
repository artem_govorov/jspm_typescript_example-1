import * as _ from 'lodash';
import {assert} from '../lang';
import {ValueModel, AbstractComputedValueModel} from "./baseValueModels";
import {valueModel} from "pectin/binding/dsl";

type ModelWithTrigger<T> = {model:ValueModel<T>, triggerModel:ValueModel<boolean>}

class Config<T> {
    alternates:ModelWithTrigger<T>[];
    defaultModel:ValueModel<T>;

    constructor() {
        this.alternates = [];
    }

    use(valueOrValueModel) {
        assert.notNull(valueOrValueModel);
        let me = this;
        return {
            when: (whenModel) => {
                assert.notNull(whenModel);
                me.alternates.push({model: valueModel.wrap(valueOrValueModel), triggerModel: whenModel});
            }
        }
    }

    otherwise(defaultValueModel) {
        this.defaultModel = valueModel.wrap(defaultValueModel);
    }
}

export class SwitchingValueModel<T,S> extends AbstractComputedValueModel<T,S> {

    static when<T>(trigger:ValueModel<boolean>) {
        return {
            use(trueModel:T|ValueModel<T>) {
                return {
                    otherwise(falseModel:T|ValueModel<T>):ValueModel<T> {
                        return new SwitchingValueModel<T, boolean>(config => {
                            config.use(trueModel).when(trigger);
                            config.otherwise(falseModel);
                        })
                    }
                }
            }
        }
    }

    private _defaultModel:ValueModel<T>;
    private _alternates:ModelWithTrigger<T>[];

    constructor(configurator) {
        super();
        var config = new Config<T>();
        configurator(config);
        assert.notNull(config.defaultModel, 'you must specify a default model using config.otherwise(...)');
        this._defaultModel = config.defaultModel;
        this.dependsOn(this._defaultModel);
        this._alternates = config.alternates;
        this._alternates.forEach(alternate => {
            this.dependsOn(alternate.triggerModel);
        })
    }

    doWrite(value) {
        this._defaultModel.value = value;
    }

    doComputeValue() {
        let value = this._defaultModel.value;
        for (let alternate of this._alternates) {
            if (alternate.triggerModel.value) {
                value = alternate.model.value;
                break;
            }
        }
        return value;
    }

}