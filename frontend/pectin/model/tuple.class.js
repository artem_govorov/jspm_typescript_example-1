var Tuple = (function () {
    function Tuple(left, right) {
        this.left = left;
        this.right = right;
    }
    return Tuple;
})();
exports.Tuple = Tuple;
