import {ValueModel} from './baseValueModels';

export class ScopeValueModel<T> extends ValueModel<T> {

    constructor(scope, $parse, expression) {
        super();
        this.scope = scope;
        this.expression = expression;
        this.reader = $parse(expression);
        this.writer = this.reader.assign;
    }

    doRead():T {
        return this.reader(this.scope);
    }

    doWrite(value:T) {
        this.writer(this.scope, value);
    }

    watch(callback, deepEquals = false) {
        this.scope.$watch(this.expression, callback, deepEquals);
        return this;
    }

}
