import * as _ from 'lodash';
import {lang, assert} from "pectin/lang";
import {AbstractComputedListModel} from 'pectin/models';
import {MutableListModel} from 'pectin/models';
import {ValueModel} from "pectin/models";
import {AbstractListModel} from "pectin/models";
import {FormModelBaseImpl} from "./formModelBase.decorator";
import {IFormModelBase} from "./formModelBase.decorator";
import {FormModel} from "./formModel.class";
import {DirtyTracking} from "../model/dirtyTracking.decorator";

@DirtyTracking()
@FormModelBaseImpl
export class AbstractFormListModel<T, S> extends AbstractListModel<T> implements MutableListModel<T> {

    private propertyName:string;
    private _form:FormModel;

    constructor(propertyName:string, form:FormModel) {
        super();
        this.propertyName = assert.notNull(propertyName);
        this._form = form;
    }

    _readProperty(sourceValue) {
        return sourceValue ? sourceValue[this.propertyName] : undefined;
    }

    _writeProperty(sourceValue, value) {
        sourceValue[this.propertyName] = value;
    }

    get form() {
        return this._form;
    }

    push(value:T) {
        this.value = this.value.concat([value]);
    }

    remove(value:T) {
        this.value = _.reject(this.values, v => v === value);
    }

}

// @see https://github.com/Microsoft/TypeScript/issues/5627#issuecomment-155955170
interface AbstractFormListModel<T,S> extends IFormModelBase<S> {}


export class FormListModel<T,S> extends AbstractFormListModel<T,S> {

    constructor(propertyName:string, sourceModel:ValueModel<S>, form:FormModel) {
        super(propertyName, form);
        this.setSourceModel(sourceModel);
    }
}
