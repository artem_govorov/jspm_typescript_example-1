//noinspection TypeScriptCheckImport
import inspect from 'object-inspect';
import * as _ from 'lodash';
import {ValueModel} from "pectin/models";
import {FormValueModel} from './formValueModel.class';
import {FormListModel} from './formListModel.class';
import {DelegatingValueModel} from "pectin/models";
import {assert,lang} from 'pectin/lang';
import {OrValueModel} from "../model/booleanValueModel.decorator";
import {IFormModelBase} from "./formModelBase.decorator";
import {IHasDirtyTracking} from "../model/dirtyTracking.decorator";
import {FormTupleModel} from "./formTupleModel.class";
import {debugContext, trace} from "../debug/debug.decorators";
import {Trace} from "../debug/trace.class";
import {after} from 'pectin/binding/dsl';

type ValueModelOptions = {dirtyTracking:boolean};
const defaultValueModelOptions = {
    dirtyTracking:true
};

export class FormModel<S> {

    private _models:Map<string, IFormModelBase & IHasDirtyTracking>;
    private _sourceModel:DelegatingValueModel<S>;

    @debugContext
    private _formEnabledModel:DelegatingValueModel<boolean>;

    private _formVisibleModel:DelegatingValueModel<boolean>;

    @debugContext
    private _dirtyModel:OrValueModel;

    constructor(sourceOrModel:S|ValueModel<S> = null) {
        this._models = new Map();

        this._formEnabledModel = new DelegatingValueModel();
        this._formEnabledModel.setDefault(true);

        this._formVisibleModel = new DelegatingValueModel();
        this._formVisibleModel.setDefault(true);

        this._dirtyModel = new OrValueModel();
        this._sourceModel = new DelegatingValueModel();
        if (sourceOrModel instanceof ValueModel) {
            this.setSourceModel(sourceOrModel);
        } else {
            this.setModel(sourceOrModel);
        }
        after.valueOf(this._sourceModel).changes(() => {
            this.clearErrors();
        });
    }

    valueModel(propertyName, options:ValueModelOptions = defaultValueModelOptions):FormValueModel {
        return this._lazyGet(propertyName, FormValueModel, options)
    }

    listModel(propertyName, options:ValueModelOptions = defaultValueModelOptions):FormListModel {
        return this._lazyGet(propertyName, FormListModel, options)
    }

    tupleModel(propertyName, options:ValueModelOptions = defaultValueModelOptions):FormTupleModel {
        return this._lazyGet(propertyName, FormTupleModel, options)
    }

    get modelObject() {
        return this._sourceModel.value;
    }

    reload() {
        this._sourceModel.fireChange();
    }

    //@trace
    enableWhen(enabledModel:ValueModel<boolean>):FormModel<S> {
        this._formEnabledModel.setDelegate(enabledModel);
        return this;
    }

    get enabledModel() {
        // not this name is used by the form model base when auto generating
        // the enable and visible models
        return this._formEnabledModel.immutable();
    }

    get visibleModel() {
        // not this name is used by the form model base when auto generating
        // the enable and visible models
        return this._formVisibleModel.immutable();
    }

    get dirtyModel() {
        return this._dirtyModel.immutable();
    }

    get dirty() {
        return this.dirtyModel.value;
    }

   //  @trace
    checkpoint() {
        Trace.log(`check pointing from: ${inspect(this._sourceModel.value)}`);
        this._dirtyModel.fireValueChangeAfter(() => {
            this._models.forEach((model, name) => {
                model.checkpoint();
            });
        });
    }

   //  @trace
    revert() {
        this._dirtyModel.fireValueChangeAfter(() => {
            this._models.forEach((model, name) => {
                model.revert();
            });
        });
    }

    setSourceModel(sourceModel:ValueModel<S>) {
        this._sourceModel.setDelegate(sourceModel);
    }

    setModel(source:S) {
        this._sourceModel.value = source;
    }

    copyTo<T>(dest:T):T {
        this._models.forEach((model, name) => {
            dest[name] = model.value;
        });
        return dest;
    }

    clearErrors() {
        this.injectErrors({})
    }

    injectErrors(errors) {
        this._models.forEach((model, name) => {
            model.setErrors(errors[name] ? errors[name] : null);
        })
    }

    dispose() {
        this.setSourceModel(null);
    }

   //  @trace
    _lazyGet<T>(propertyName, ctor:T, options:ValueModelOptions):T {
        if (!this._models.has(propertyName)) {
            let model = new ctor(propertyName, this._sourceModel, this);
            model.checkpoint(); // establish our dirty baseline.
            this._models.set(propertyName, model);
            if (options.dirtyTracking) {
                this._dirtyModel.addSource(model.dirtyModel);
            }
            //Trace.log(`enabledModel.value = ${this.enabledModel.value}`);
            //Trace.log(`enabled = ${model.enabled}`);
        }
        return this._models.get(propertyName);
    }
}

