import _ from 'lodash';
//noinspection TypeScriptCheckImport
import inspect from 'object-inspect';
import {lang, assert, ConstructorFunction} from '../lang';
import {HasValue} from '../model/abstractHasValue.class';
import {ValueModel} from '../models';
import {ValueHolder} from '../models';
import {PropertyValueModel} from '../models';
import {PropertyListModel} from '../models';
import {AbstractListModel} from '../models';
import {ListModel} from '../models';
import {ComputedValueModel} from '../models';
import {ComputedBooleanValueModel} from '../models';
import {ConvertingValueModel} from '../models';
import {ComputedListModel} from '../models';
import {MappedListModel} from '../models';
import {SortedListModel} from '../models';
import {NotValueModel, OrValueModel} from "../model/booleanValueModel.decorator";
import {IBooleanValueModel} from "../model/booleanValueModel.decorator";
import {AndValueModel} from "../model/booleanValueModel.decorator";
import {StickyValueModel, DebouncedValueModel} from "../model/baseValueModels";
import {ListHolder} from "../model/baseListModels";
import {Filter} from "../../app/component/search/filter.class";
import {Index, ScalarIndex, ListIndex} from "../model/index.class";
import {Disposable, IDisposable, GarbageCollector} from "./disposable.class";
import {Trace} from "../debug/trace.class";
import {BeforeChangeListener} from "../model/hasValueChangeListeners.class";
import {trace} from "../debug/debug.decorators";
import {ModelBinding} from "./modelBinding.class";
import {Converter} from "../model/converter.class";



function assertListModel(object) {
    if (!(object instanceof ListModel)) {
        throw lang.illegalState('expected instance of ListModel')
    }
    return object;
}

function assertValueModel(object) {
    if (!(object instanceof ValueModel)) {
        throw lang.illegalState('expected instance of ValueModel')
    }
    return object;
}

export var valueModel = {
    fromProperty: function (target, propertyName) {
        return new PropertyValueModel(propertyName).bindTo(target);
    },
    computedFrom: <T, S>(source:HasValue<S>) => {
        return {
            using: (func:(S)=>T):ComputedValueModel<T, S> => new ComputedValueModel<T,S>(source, func)
        }
    },
    computedFromAll: <any, any>(...sources: HasValue<any>[]) => {
        return {
            using: (func: ()=>any): ComputedValueModel<any, any> => {
                const computedValueModel = new ComputedValueModel<any,any>(sources.shift(), func);
                sources.forEach(s => computedValueModel.dependsOn(s));
                return computedValueModel
            }
        }
    },
    selectedFrom: (listModel) => {
        return {
            when: (predicate) => new ComputedValueModel(listModel, list => {
                for (let item of list) {
                    if (predicate(item)) {
                        return item;
                    }
                }
                return null;
            }),
            using: (criteriaModel) => {
                return {
                    when: (predicate) => {
                        let model = new ComputedValueModel(listModel, list => {
                            for (let item of list) {
                                if (predicate(item, criteriaModel.value)) {
                                    return item;
                                }
                            }
                            return null;
                        });
                        model.dependsOn(criteriaModel);
                        return model;
                    }
                }
            }
        }
    },
    pickedFrom: <T,S>(listModel) => {
        return {
            using: (func:(list:S[])=>T):ComputedValueModel<T, S[]> => new ComputedValueModel<T,S[]>(listModel, func)
        }
    },
    firstOf: <S>(listModel) => {
        return new ComputedValueModel<S,S[]>(listModel, (items:S) => _.first(items));
    },
    lastOf: <S>(listModel) => {
        return new ComputedValueModel<S,S[]>(listModel, (items:S) => _.last(items));
    },
    wrap: (valueOrModel) => {
        return (valueOrModel instanceof ValueModel) ? valueOrModel : new ValueHolder(valueOrModel);
    },
    debounce: <T>(valueModel) => {
        return new DebouncedValueModel<T>(valueModel);
    },
    // sticky value models will hold onto a value if the source becomes null.
    sticky: <T>(valueModel:ValueModel<T>) => {
        return new StickyValueModel<T>(valueModel)
    },
    convertedFrom: <T,S>(source:ValueModel<S>) => {
        return {
            using(converter:Converter<T,S>) {
                return new ConvertingValueModel(source, converter);
            }
        }
    }
};

export var listModel = {

    fromProperty: (target, propertyName) => new PropertyListModel(propertyName).bindTo(target),
    computedFrom: <T,R>(source:ValueModel<T> | ListModel<T>) => { // TODO: make this explicitly handle value or list models.  it's current accidentally working.
        return {
            using: (func:((v:T)=>R)) => new ComputedListModel<R,T>(source, func),
            concat: (arrayOrValueModel) => {
                return new ComputedListModel(source, (values) => {
                    return [].concat(values, arrayOrValueModel.value);
                })
            }
        }

    },
    select: (source) => {
        return {
            when: (predicate) => {
                return listModel.computedFrom(source).using(values => {
                    return _.filter(values, predicate);
                });
            },
            using: (criteriaModel) => {
                return {
                    when: (predicate) => {
                        let model = listModel.computedFrom(source).using(values => {
                            return _.filter(values, (value) => predicate(value, criteriaModel.value));
                        });
                        model.dependsOn(criteriaModel);
                        return model;
                    }
                }
            }
        }
    },
    // TODO: move this to a FilteredListModel that has add.filter(filterOrFilterModel).
    dynamicallyFilter: <T>(source:ListModel<T>) => {
        return {
            using: (filterCriteriaModel:HasValue<Filter<T>>) => {
                return listModel.select(source).using(filterCriteriaModel).when((value, filter) => !filter || filter.matches(value))
            },
            usingAll: (...filterCriteriaModels: HasValue<Filter<T>>[]) => {
                let list = source;
                filterCriteriaModels.forEach((filterCriteriaModel) => {
                    list = listModel.dynamicallyFilter(list).using(filterCriteriaModel)
                });
                return list;
            }
        }
    },
    from: (source) => {
        return {
            reject: (predicate) => {
                return listModel.computedFrom(source).using(values => {
                    return _.reject(values, predicate);
                });
            }
        }
    },
    // use ListModel#map instead.. that way models can return instances with properties/capabilities of the original model.
    // map: (source) => {
    //     return {using: (func) => new MappedListModel(source, func)}
    // },
    sort: <T>(source:ListModel<T>) => {
        return {
            byTextAttribute: (...attributes) => new SortedListModel<T>(source, (values) => {
                return values.sort((a, b) => {
                    for (let attr of attributes) {
                        let aAttr = a[attr] || '';
                        let bAttr = b[attr] || '';
                        const result = aAttr.localeCompare(bAttr, undefined /* Ignore language */, {sensitivity: 'base'});
                        if (result != 0) return result;
                    }
                    // See http://stackoverflow.com/a/29830220/1248812
                    return 0;
                });
            }),
            byAttribute: (attribute) => new SortedListModel<T>(source, (values) => _.sortBy(values, attribute))
        }
    },
    wrap: (array) => {
        if (array instanceof AbstractListModel) return array;

        assert.isArray(array);
        var model = new ListHolder();
        model.value = array;
        return model;
    },
    chunk: (source) => {
        return {
            intoGroupsOf: (columns) => {
                return listModel.computedFrom(source).using(optionList => _.chunk(optionList, columns));
            }
        }
    }
    // collectedFromChangesTo<T>(valueModel:ValueModel<T>):ListModel<T> {
    //     const list = new ListHolder();
    //     valueModel.onValueChange(v => {list.push(v)});
    //     return list;
    // }
};


// todo: Make valueOf and not be on a when object so the subsequent boolean methods chains can be easily implied.
// E.g. let editingEnabled = when.not(this.busySaving).and.valueOf(this.selectionModel).isNotEmpty()
export function valueOf<S>(source:ValueModel<S>) {
    return {
        isNotEmpty: ():ComputedBooleanValueModel => {
            let model = new ComputedBooleanValueModel(source, (value) => !!value);
            model.computeWhenSourceOrValueIsNull();
            return model;
        },
        isEmpty: ():ComputedBooleanValueModel => {
            let model = new ComputedBooleanValueModel(source, (value) => {
                return _.isNull(value) || _.isUndefined(value)
            });
            model.computeWhenSourceOrValueIsNull();
            return model
        },
        isBlank: ():ComputedBooleanValueModel => {
            let model = new ComputedBooleanValueModel(source, (value) => {
                return _.isNull(value) || _.isUndefined(value) || value == ""
            });
            model.computeWhenSourceOrValueIsNull();
            return model
        },
        isTruthy: ():ComputedBooleanValueModel => {
            let model = new ComputedBooleanValueModel(source, (value) => !!value);
            model.computeWhenSourceOrValueIsNull();
            return model
        },
        isFalsey: ():ComputedBooleanValueModel => {
            let model = new ComputedBooleanValueModel(source, (value) => !value);
            model.computeWhenSourceOrValueIsNull();
            return model
        },
        isInstanceOf: (compareToValue:S):ComputedBooleanValueModel => {
            let compareToModel = valueModel.wrap(compareToValue);
            let model = new ComputedBooleanValueModel(source, (value) => {
                //console.log(`${value}==${compareToValue} = ${value == compareToValue}`);
                return value instanceof compareToModel.value
            });
            model.computeWhenSourceOrValueIsNull();
            model.dependsOn(compareToModel);
            return model
        },
        equals: (compareToValue:S):ComputedBooleanValueModel => {
            let compareToModel = valueModel.wrap(compareToValue);
            let model = new ComputedBooleanValueModel(source, (value) => {
                //console.log(`${value}==${compareToValue} = ${value == compareToValue}`);
                return value == compareToModel.value
            });
            model.computeWhenSourceOrValueIsNull();
            model.dependsOn(compareToModel);
            return model
        },
        satisfies: (predicate:(value:S)=>boolean):ComputedBooleanValueModel => {
            let model = new ComputedBooleanValueModel(source, (value) => predicate(value));
            model.computeWhenSourceOrValueIsNull();
            return model
        },
        isGreaterThan: (compareToValue:S|ValueModel<S>):ComputedBooleanValueModel => {
            let compareToModel = valueModel.wrap(compareToValue);
            let model = new ComputedBooleanValueModel(source, (value) => {
                //console.log(`${value}==${compareToValue} = ${value == compareToValue}`);
                return value > compareToModel.value;
            });
            model.computeWhenSourceOrValueIsNull();
            model.dependsOn(compareToModel);
            return model
        },
        isGreaterThanOrEqualTo: (compareToValue:S|ValueModel<S>):ComputedBooleanValueModel => {
            let compareToModel = valueModel.wrap(compareToValue);
            let model = new ComputedBooleanValueModel(source, (value) => {
                // console.log(`${value}>=${compareToValue} = ${value >= compareToValue}`);
                return _.isNumber(value) ? value >= compareToModel.value : false;
            });
            model.computeWhenSourceOrValueIsNull();
            model.dependsOn(compareToModel);
            return model
        },
        isLessThan: (compareToValue:S|ValueModel<S>):ComputedBooleanValueModel => {
            let compareToModel = valueModel.wrap(compareToValue);
            let model = new ComputedBooleanValueModel(source, (value) => {
                // console.log(`${value}==${compareToValue} = ${value == compareToValue}`);
                return value < compareToModel.value;
            });
            model.computeWhenSourceOrValueIsNull();
            model.dependsOn(compareToModel);
            return model
        },
        isLessThanOrEqualTo: (compareToValue:S|ValueModel<S>):ComputedBooleanValueModel => {
            let compareToModel = valueModel.wrap(compareToValue);
            let model = new ComputedBooleanValueModel(source, (value) => {
                //console.log(`${value}==${compareToValue} = ${value == compareToValue}`);
                return _.isNumber(value) ? value <= compareToModel.value : false;
            });
            model.computeWhenSourceOrValueIsNull();
            model.dependsOn(compareToModel);
            return model
        },
        doesNotEqual: (compareToValue:S):IBooleanValueModel => {
            return not(valueOf(source).equals(compareToValue))
        },
        isNot: (compareToValue:S):IBooleanValueModel => {
            return valueOf(source).doesNotEqual(compareToValue);
        },
        or: (other:ValueModel<boolean>) => {
            return new OrValueModel([source, other])
        },
        orNot: (other:ValueModel<boolean>) => {
            return new OrValueModel([source, not(other)])
        },
        and: (other:ValueModel<boolean>) => {
            return new AndValueModel([source, other])
        },
        andNot: (other:ValueModel<boolean>) => {
            return new AndValueModel([source, not(other)])
        },
        matches<S>(predicate:(v:S)=>boolean):ValueModel<boolean> {
            const valueModel = new ComputedBooleanValueModel(source, predicate);
            valueModel.computeWhenSourceOrValueIsNull();
            return valueModel;
        }

    }
}

export function listOf<S>(source:ListModel<S>) {
    return {
        isNotEmpty: (): ComputedBooleanValueModel => {
            return listOf(source).isPopulated();
        },
        isPopulated: (): ComputedBooleanValueModel => {
            return new ComputedBooleanValueModel(source, (items:S[]) => items && items.length > 0)
                .computeWhenSourceOrValueIsNull();
        },
        isEmpty: (): ComputedBooleanValueModel => {
            return new ComputedBooleanValueModel(source, (items:S[]) => {
                return _.isNull(items) || _.isUndefined(items) || items.length < 1
            }).computeWhenSourceOrValueIsNull();
        }
    }
}


abstract class AbstractConditionalChangeAdapter<S> implements IDisposable {

    protected predicate: (value?: S)=>boolean = () => true;
    protected registration: IDisposable;

    ['if'](predicate: (value?: S)=>boolean|ValueModel<boolean>): IDisposable {
        this.predicate = this.asPredicate(predicate);
        return this;
    }

    unless(unlessPredicate: (value?: S)=>boolean|ValueModel<boolean>): IDisposable {
        const p = this.asPredicate(unlessPredicate);
        this.predicate = (value?:S) => !p(value);
        return this;
    }

    dispose(): void {
        this.registration.dispose();
    }

    protected asPredicate(predicate: (value?: S)=>boolean|ValueModel<boolean>) {
        return _.isFunction(predicate)
            ? predicate
            : () => predicate['value'];
    }
}

class ConditionalBeforeChangeAdapter<S> extends AbstractConditionalChangeAdapter<S> {

    constructor(source:ValueModel<S>, func:(oldValue?:S)=>void) {
        super();
        this.registration = source.onBeforeValueChange(oldValue => {
            if (this.predicate(oldValue)) {
                func(oldValue);
            }
        });
    }
}
class ConditionalChangeAdapter<S> extends AbstractConditionalChangeAdapter<S> {

    constructor(source: ValueModel<S>, func: (newValue: S, oldValue?: S)=>void) {
        super();
        this.registration = source.onValueChange((newValue, oldValue) => {
            if (this.predicate(newValue)) {
                func(newValue, oldValue);
            }
        });
    }
}

class ConditionalAfterChangeAdapter<S> extends AbstractConditionalChangeAdapter<S> {

    constructor(source: ValueModel<S>, func: (newValue: S, oldValue?: S)=>void) {
        super();
        this.registration = source.onAfterValueChange((newValue, oldValue) => {
            if (this.predicate(newValue)) {
                func(newValue, oldValue);
            }
        });
    }
}

class BaseDslChangeTransitions<S, H extends AbstractConditionalChangeAdapter> {

    constructor(protected garbageCollector:GarbageCollector,
                protected source: HasValue<S>,
                protected changeHandlerConstructor: typeof H) {
    }

    changes(f: (newValue: S, oldValue?: S)=>void): H {
        return <H>this.garbageCollector.registerDisposable(new this.changeHandlerConstructor(this.source, f));
    }
}

class DslValueChangeTransitions<S, H extends AbstractConditionalChangeAdapter> extends BaseDslChangeTransitions<S,H> {

    becomesTruthy(f: (newValue?: S, oldValue?: S)=>void): H {
        return this.matches((newValue, oldValue) => !!newValue && !oldValue).invoke(f);
    }

    becomesFalsey(f: (newValue?: S, oldValue?: S)=>void): H {
        return this.matches((newValue, oldValue)=> !newValue && !!oldValue).invoke(f);
    }

    changesTo(desiredValue: S) {
        return this.matches((newValue, oldValue) => _.isEqual(newValue, desiredValue) && !_.isEqual(oldValue, desiredValue))
    }

    matches(predicate: (newValue: S, oldValue?: S)=>boolean) {
        const me = this;
        const gc = this.garbageCollector;
        return {
            invoke(f: ()=>void): H {
                const predicatedCallback = (newValue: S, oldValue: S) => {
                    if (predicate(newValue, oldValue)) {
                        f();
                    }
                };
                return <H>gc.registerDisposable(new me.changeHandlerConstructor(me.source, predicatedCallback));
            }
        }
    }
}

class DslListChangeTransitions<S, H extends AbstractConditionalChangeAdapter> extends BaseDslChangeTransitions<S,H> {
    // becomesEmpty() {
    //
    // }
    // becomesPopulated() {
    //
    // }
    // includes(predicate) {
    //
    // }
    // matches(predicate) {
    //
    // }
}


// export var before = {
//     valueOf<S>(source: ValueModel<S>) {
//         return new BaseDslChangeTransitions(source, ConditionalBeforeChangeAdapter);
//     },
//     listOf(source:ListModel<S>) {
//         return new BaseDslChangeTransitions(source, ConditionalBeforeChangeAdapter);
//     }
// };
//
// export var after = {
//     valueOf<S>(source: ValueModel<S>) {
//         return new DslValueChangeTransitions(source, ConditionalAfterChangeAdapter);
//     },
//     listOf<S>(source:ListModel<S>) {
//         return new DslListChangeTransitions(source, ConditionalAfterChangeAdapter);
//     }
// };
//
// export var when = {
//     valueOf<S>(source: ValueModel<S>) {
//         return new DslValueChangeTransitions(source, ConditionalChangeAdapter);
//     },
//     listOf<S>(source:ListModel<S>) {
//         return new DslListChangeTransitions(source, ConditionalChangeAdapter);
//     }
// };

// let {when, valueOf, after, before} = Dsl.for(this);

const dslProperty = Symbol.for("pectin:dsl");

export class Dsl {

    static get(garbage: GarbageCollector):Dsl {
        if (!garbage[dslProperty]) {
            garbage[dslProperty] = new Dsl(garbage);
        }
        return garbage[dslProperty];
    }

    private constructor(private garbage: GarbageCollector) {
    }

    get when() {
        return {
            valueOf<S>(source: ValueModel<S>) {
                return new DslValueChangeTransitions(this.garbage, source, ConditionalChangeAdapter);
            },
            listOf<S>(source: ListModel<S>) {
                return new DslListChangeTransitions(this.garbage, source, ConditionalChangeAdapter);
            }
        }
    }

    get before() {
        return {
            valueOf<S>(source: ValueModel<S>) {
                return new BaseDslChangeTransitions(this.garbage, source, ConditionalBeforeChangeAdapter);
            },
            listOf<S>(source: ListModel<S>) {
                return new BaseDslChangeTransitions(this.garbage, source, ConditionalBeforeChangeAdapter);
            }
        };
    }

    get after() {
        return {
            valueOf<S>(source: ValueModel<S>) {
                return new DslValueChangeTransitions(this.garbage, source, ConditionalAfterChangeAdapter);
            },
            listOf<S>(source: ListModel<S>) {
                return new DslListChangeTransitions(this.garbage, source, ConditionalAfterChangeAdapter);
            }
        }
    }
}

// export function listOf<S>(source:ListModel<S>) {
//     isEmpty: () => {
//
//     }
// }


export function index<T>(listModel:ListModel<T>) {
    return {
        on(key:string) {
            return {
                asUnique():ScalarIndex<T> {
                    return ScalarIndex.getOrCreate(listModel, key);
                },
                asHasMany():ListIndex<T> {
                    return ListIndex.getOrCreate(listModel, key);
                }
            };
        }
    }
}

export function not<T>(model:ValueModel<T>):IBooleanValueModel {
    return new NotValueModel(model);
}

export function and(...models:ValueModel<boolean>[]):IBooleanValueModel {
    return new AndValueModel(models);
}

export function or(...models:ValueModel<boolean>[]):IBooleanValueModel {
    return new OrValueModel(models);
}

export function anyOf(...models:ValueModel<boolean>[]) {
    const modelArray = _.flattenDeep(models);
    return {
        areTrue() {
            return new OrValueModel(modelArray);
        }
    }
}


class MultiplyingValueModel extends ConvertingValueModel<number, number> {

    private amount:number;

    constructor(sourceModel:ValueModel<number>, amount:number) {
        super(sourceModel);
        this.amount = assert.isNumber(amount);
    }

   //  @trace
    fromSource(value:number):number {
        return value * this.amount;
    }

   //  @trace
    toSource(value:number):number {
        return value / this.amount;
    }
}

export function multiply(sourceModel:ValueModel<number>) {
    return {
        by: (amount:number):ValueModel<number> => {
            const result = new ValueHolder<number>();
            ModelBinding.bind(result, sourceModel, {
                fromSource: (v) => v*amount,
                toSource: (v) => v/amount
            });
            return result;
        }
    }
}






