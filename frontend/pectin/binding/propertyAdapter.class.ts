import {lang, assert} from '../lang';
import {ValueModel} from '../models';

export class PropertyAdapter extends ValueModel {

    constructor(model, target, propertyName) {
        super();
        assert.notNull(model);
        this.target = assert.notNull(target);
        this.propertyName = assert.notNull(propertyName);

        let propertyConfiguration = {
            enumerable: true,
            configurable: true,
            get: () => {
                return model.value
            },
            set: (value) => {
                model.value = value
            }
        };
        Object.defineProperty(this.target, propertyName, propertyConfiguration);
    }

    dispose() {
        delete this.target[this.propertyName];
    }

}
