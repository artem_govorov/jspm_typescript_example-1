import * as _ from 'lodash';

export class AbstractTreeNode<T extends AbstractTreeNode>  {

    protected parent: T;
    protected children: T[] = [];

    descendants():AbstractTreeNode[] {
        let result = [];
        this.visitChildren(child => result.push(child));
        return result;
    }

    selfAndDescendants():AbstractTreeNode[] {
        let result = [];
        this.visitAll(child => result.push(child));
        return result;
    }

    siblingsBefore():AbstractTreeNode[] {
        if (this.parent) {
            let myIndex = this.parent.children['indexOf'](this);
            return this.parent.children.slice(0, myIndex);
        } else {
            return [];
        }
    }

    siblingsAfter():AbstractTreeNode[] {
        if (this.parent) {
            let myIndex = this.parent.children['indexOf'](this);
            return this.parent.children.slice(1 + myIndex);
        } else {
            return [];
        }
    }

    siblings():AbstractTreeNode[] {
        return [].concat(this.siblingsBefore(), this.siblingsAfter());
    }

    isFirstChild():boolean  {
        return this.parent ? this.parent.children['indexOf'](this) == 0 : false;
    }

    isLastChild():boolean  {
        return this.parent ? this.parent.children['indexOf'](this) == this.parent.children.length - 1 : false;
    }

    hasSiblings():boolean {
        return this.parent ? this.parent.children.length > 1 : false;
    }

    find(predicate) {
        if (!(typeof predicate === 'function')) throw Error(`predicate must be a function, got ${typeof predicate}`);
        return _.find(this.descendants(), predicate);
    }

    select(predicate) {
        if (!(typeof predicate === 'function')) throw Error(`predicate must be a function, got ${typeof predicate}`);
        return _.filter(this.descendants(), predicate);
    }

    flattenInto(array) {
        this.visitAll(child => array.push(child));
    }

    flatten():AbstractTreeNode[] {
        let result = [];
        this.flattenInto(result);
        return result;
    }

    visitAll(visitor) {
        visitor.call(this, this);
        this.visitChildren(visitor)
    }

    visitChildren(visitor) {
        _.each(this.children, child => child.visitAll(visitor));
    }

    ancestors(): AbstractTreeNode[] {
        return this.parent ? this.parent.ancestors().concat([this.parent]) : []
    }

    rootAncestor():AbstractTreeNode {
        return _.first(this.ancestors());
    }

    ancestorsAndSelf():AbstractTreeNode[] {
        return this.ancestors().concat([this]);
    }

    pathToParent(parent: AbstractTreeNode) {
        if (this === parent) {
            return []
        }
        else if (this.parent) {
            return this.parent.pathToParent(parent).concat([this.parent])
        }
        else {
            throw new Error("parent is not an ancestor");
        }
    }

    pathToParentAndSelf(parent) {
        return this.pathToParent(parent).concat([this]);
    }

    isRoot(): boolean {
        return !this.parent;
    }

    isLeaf(): boolean {
        return !this.isRoot() && this.children.length < 1;
    }

    hasChildren(): boolean {
        return !this.isLeaf();
    }

    // treeDepth():number {
    //     return 1 + this.descendants().map(child => child.depthFromParent(this)).reduce((prev, current) => Math.max(prev, current));
    // }

}