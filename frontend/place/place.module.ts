import angular from 'angular';
//noinspection TypeScriptCheckImport
import inspect from 'object-inspect';
import {FocusUtil} from 'components/focusUtil.class.js';
import IWindowService = angular.ng.IWindowService;
import {StateService} from "angular-ui-router";
import {TransitionService} from "angular-ui-router";
import {Transition} from "angular-ui-router";

import IDocumentService = angular.ng.IDocumentService;
import IRootScopeService = angular.ng.IRootScopeService;


var placeModule = angular.module('la.ui.places', []);

// placeModule.constant('rootPlace', new RootPlace());
//
// placeModule.config((rootPlace, $stateProvider) => {
//     rootPlace.initialize($stateProvider);
// });
//
// placeModule.run(($injector: angular.auto.IInjectorService,
//                  $transitions: TransitionService,
//                  $rootScope: IRootScopeService,
//                  $state: StateService,
//                  $window: IWindowService,
//                  $document: IDocumentService) => {
//
//
// });


export default placeModule;