import {assert, lang} from 'pectin/lang';
import {Place} from "./place.class";
import {ConstructorFunction} from "pectin/lang";
import {VisitMetadata} from "./placeDecorators";
import {PlaceBuilder} from "./placeBuilder.class";
// import {VisitHelper} from "./visitMetadata";

export class PlaceDefinition {

    private _visitType: ConstructorFunction;
    private _componentType: ConstructorFunction;
    private _templateProvider: (...args: any[])=>string;
    private _routeIdentifier: string;
    private _componentAs:string;

    constructor(private _name: string) {
    }

    get componentClass():ConstructorFunction<any> {
        return this._componentType;
    }

    get componentAs() {
        return this._componentAs;
    }

    get visitClass():ConstructorFunction<any> {
        return this._visitType;
    }

    get name(): string {
        return this._name;
    }

    get templateProvider():(...args:any[])=>string {
        return this._templateProvider;
    }

    /**
     * Returns a variable name based on the visit class name.  For the visit class of BlahVisit this method
     * will return $blahVisit.
     * @returns {string}
     */
    get visitParameterName(): string {
        const name = this._visitType.name;
        return `$${name.charAt(0).toLowerCase()}${name.substr(1)}`
    }

    get routeIdentifierPropertyName(): string {
        return this._routeIdentifier;
    }

    protected setVisitType(type: ConstructorFunction<any>) {
        this._visitType = type;
        this._routeIdentifier = Reflect.getMetadata(VisitMetadata.RouteIdentifier, this._visitType);
    }

    protected setComponent(type: ConstructorFunction<any>, options:PlaceBuilder.ComponentOptions) {
        assert.notBlank(options.template || options.templateUrl, 'must provide either a template or templateUrl');
        this._componentType = type;
        this._componentAs = options.as || 'ctrl';
        this._templateProvider = options.templateUrl
            ? ($http, $templateCache) => $http.get(options.templateUrl, {cache: $templateCache}).then(response => response.data)
            : () => options.template;
    }



}