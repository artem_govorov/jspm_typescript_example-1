// import {ConstructorFunction} from "pectin/lang";
import {Reflect} from "reflect-metadata";

export enum PlaceMetadata {
    AuthenticationType,
    Public,
    Authenticated
}

export function Public(targetClass) {
    Reflect.defineMetadata(PlaceMetadata.AuthenticationType, PlaceMetadata.Public, targetClass)
}

export function Authenticated(targetClass) {
    Reflect.defineMetadata(PlaceMetadata.AuthenticationType, PlaceMetadata.Authenticated, targetClass)
}
