import {ConstructorFunction} from "../pectin/lang";
import {Place} from "./place.class";
import {PlaceDefinition} from "./placeDefinition.class";
import ComponentOptions = PlaceBuilder.ComponentOptions;

export interface PlaceBuilder {
    visitType(type: ConstructorFunction<any>): PlaceBuilder
    component(type: ConstructorFunction<any>, options:ComponentOptions): PlaceBuilder
    create(): Place
}


export class DefaultPlaceBuilder extends PlaceDefinition implements PlaceBuilder {

    create(): Place {
        return new Place(this);
    }

    visitType(type: ConstructorFunction<any>): PlaceBuilder {
        this.setVisitType(type);
        return this;
    }

    component(type: ConstructorFunction<any>, options: PlaceBuilder.ComponentOptions): PlaceBuilder {
        this.setComponent(type, options);
        return this;
    }
}

export namespace PlaceBuilder {
    export interface ComponentOptions {
        template?: string;
        templateUrl?: string;
        as?: string;
    }
}