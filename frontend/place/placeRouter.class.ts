import {PlaceDefinition} from "./placeBuilder.class";
import {Place} from "./place.class";
import {AuthService} from "./authService.class";

export interface PlaceRouter {
    installRoutes(place: Place): void;
}