import {assert, lang, ConstructorFunction} from 'pectin/lang';
import * as _ from 'lodash';
//noinspection TypeScriptCheckImport
import inspect from 'object-inspect';

export class VisitQuery {
    private _history: any[];
    private _type: ConstructorFunction;
    private _predicate: (visit: any)=>boolean;
    private _limit: number;

    constructor(history, type, predicate = null, limit = 100) {
        this._history = history;
        this._type = type;
        this._predicate = predicate || (visit=>true);
        this._limit = limit;
    }

    last(): any {
        return _.findLast(this._history, visit => this._isPredicateMatch(visit))
    }

    find(): any[] {
        return _.filter(this._history, visit => this._isPredicateMatch(visit)).slice(-this._limit);
    }

    limit(limit:number): VisitQuery {
        return new VisitQuery(this._history, this._type, this._predicate, limit);
    }

    matching(predicate: (visit: any)=>boolean): this {
        this._predicate = predicate;
        return this;
    }

    _isPredicateMatch(visit: any) {
        return (visit instanceof this._type) && this._predicate(visit);
    }


}
export class VisitHistory {
    private _history: any[];

    constructor() {
        this._history = [];
    }

    add(visit: any) {
        console.log(`--> ${this.constructor.name}.add`);
        this._history.push(visit);
        console.log(`<-- `);
    }

    lastVisitOfType(type: ConstructorFunction): any {
        return this.lastVisitMatching(visit => (visit instanceof type))
    }

    lastVisitMatching(predicate: (visit: any)=>boolean) {
        return _.findLast(this._history, visit => predicate(visit))
    }

    queryVisitsOfType(type: ConstructorFunction): VisitQuery {
        return new VisitQuery(this._history, type);
    }

    previousVisit(): any {
        return _.last(this._history);
    }

}