module.exports = function (wallaby) {

    return {
        files: [
            // system.js and configuration
            {pattern: 'jspm_packages/system.js', instrument: false},
            {pattern: 'jspm.config.js', instrument: false},
            {pattern: 'tsconfig.json', instrument: false},

            // source files (`load: false` as the files will be loaded by system.js loader)
            {pattern: 'place/**/*.ts', load: false}
        ],
        tests: [
            // test files (`load: false` as we will load tests manually)
            {pattern: 'tests/**/*.spec.ts', load: false}
        ],
        testFramework: 'mocha',

        // npm install electron --save-dev
        env: {
            kind: 'electron',
            options: { width: 1200, height: 800 }
        },

        // telling wallaby to serve jspm_packages project folder as is from wallaby web server
        middleware: (app, express) => {
            app.use('jspm_packages', express.static(require('path').join(__dirname, 'jspm_packages')));
        },

        setup: function (wallaby) {
            wallaby.delayStart();
            // System.config({
            //     browserConfig: {
            //         "baseURL": "."
            //     },
            //     transpiler: "ts",
            //     typescriptOptions: {
            //         "tsconfig": true
            //     }
            // });

            const promises = [];
            wallaby.tests.forEach(test => {
                // promises.push(System['import'](test.replace(/\.js$/, '')));
                promises.push(System['import'](test));
            });

            Promise.all(promises).then(() => {
                wallaby.start();
            }).catch(function (e) {
                setTimeout(function () {
                    throw e;
                }, 0);
            });
        }
    }
};