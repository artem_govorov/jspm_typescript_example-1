ApiActions.setup do |config|

  # Specify the directories where the api action controllers live.  This allows the system
  # to preload the classes in development so that the routes can be configured.
  config.controller_module = 'Api'

  # Where rails api_actions:generate_schema will output the schema.
  config.schema_generation_file = Rails.root.join('frontend/api_actions/gen/schema.json')
  config.template_output_directory = Rails.root.join('frontend/app/api_actions/dao')

end

