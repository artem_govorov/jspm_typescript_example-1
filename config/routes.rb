Rails.application.routes.draw do
  get 'concert_manager/index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # Serve websocket cable requests in-process
  # mount ActionCable.server => '/cable'


  # You can have the root of your site routed with "root"
  root 'concert_manager#index'

  scope '/api', constraints: ApiConstraints.new(version: 1, default: true), defaults: {format: :json} do
    ApiActions.install_routes(self)
  end


  # Missing routes got to root.  No idea why I need '*path' rather than just '*'.
  get '*path', to: redirect('/')

end
